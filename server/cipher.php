<?php
	function cipher($text, $password, $option){
		$method = 'aes-256-cbc';
		$key = pack("H*",hash("sha256",$password));
		$iv = pack("H*",md5($password));
		switch($option) {
			case "encrypt":
				return base64_encode(openssl_encrypt($text, $method, $key, OPENSSL_RAW_DATA, $iv));
				break;
			case "decrypt":
				return openssl_decrypt(base64_decode($text), $method, $key, OPENSSL_RAW_DATA, $iv);
				break;
			default:
				return NULL;
				break;
		}
	}

	function hashPwd($text,$user){
		$salt = "PaR4l!p0M£ni_Del|a~&aTr4cOmIoMAcHia";
		return hash("sha256",$user.$salt.$text);
	}
?>
