<?php

	function changeAvatar(){
		echo json_encode(putPic($_REQUEST['user'], $_REQUEST['pic']));
	}

	function changeName($conn)
	{
		//id, nuovo nome
		$idUser = $_REQUEST['userId'];
		$newName = $_REQUEST['newName'];
		echo json_encode(mysqli_query($conn,"UPDATE user SET name = '$newName' WHERE id = '$idUser'"));
	}

	function changePwd($conn)
	{
		//id, nuova e vecchia pwd
		$idUser = $_REQUEST['userId'];
		$oldPwd = $_REQUEST['oldPwd'];
		$newPwd = $_REQUEST['newPwd'];
		//hash password vecchia per confronto
		$res=mysqli_query($conn, "SELECT username,pwd FROM user WHERE id = '$idUser'");
		//confronto password
		$old = mysqli_fetch_assoc($res);
		$user = $old['username'];
		$oldPwd = hashPwd($oldPwd,$user);
		if($oldPwd == $old['pwd'])
		{
			$newPwd = hashPwd($newPwd,$user);
			echo json_encode(mysqli_query($conn,"UPDATE user SET pwd = '$newPwd' WHERE id = '$idUser'"));
			// aggiorno avatar
			$pic = cipher(file_get_contents('data/pics/'.$user),$oldPwd,'decrypt');
			file_put_contents('data/pics/'.$user,cipher($pic,$newPwd,'encrypt'));
		}else{
			echo json_encode('error');
		}

	}
	
	function getGroupsAdmin($conn)  //Ritorna tutti i gruppi di cui un utente è admin 
	{
		$userId=$_REQUEST['idUser'];
		$query="SELECT id,nome,descrizione,pwd FROM gruppi WHERE id_creatore = '$userId' ";
		$res=mysqli_query($conn,$query);
		if (!$res)
		{
			echo json_encode('error');
			return;
		}
		$arr=array();
		$ret=array();
		$i=0;
		while($arr=mysqli_fetch_assoc($res)){
			$ret[$i]['id']=$arr['id'];
			$ret[$i]['nome']=$arr['nome'];
			$ret[$i]['pwd']=true;
			if ($arr['pwd']=='')	$ret[$i]['pwd']=false;
			$ret[$i]['description']=$arr['descrizione'];
			$i++;
		}
		echo json_encode($ret); 
	}

	function changeGroupDescription($conn)  //Cambia la descrizione di un gruppo, dato idGruppo e nuova descrizione
	{
		$groupId=$_REQUEST['idGroup'];
		$descrizione=$_REQUEST['groupDescription'];
		$query="UPDATE gruppi SET descrizione='$descrizione' WHERE id = '$groupId'";
		echo json_encode(mysqli_query($conn,$query)); 
	}

	function deleteUser($conn,$conn_msg)	//Cancella user, dato l'id. Inoltre cancella le relazioni in 'appartiene'
	{
		$userId=$_REQUEST['idUser'];
		$SU=$_REQUEST['SU']; //1 se il SU richiede l'eliminazione, 0 se lo richiede l'utente stesso
		$ret=true;
		if($SU==1)
		{	 
			//controllo se idUser appartiene a un SU
			$query = "SELECT superuser FROM user WHERE id = '$userId'";
			$arr = mysqli_fetch_assoc(mysqli_query($conn,$query));
			$superuser = $arr['superuser'];
			if ($superuser==1)
			{
				echo json_encode('error'); //un superuser non può essere eliminato da un altro SU
				return;
			}
			$ret=mailDeleteUserSU($conn, $userId);
		}
		
		//elimina avatar
		$query="SELECT username FROM user WHERE id = '$userId' ";
		$res=mysqli_query($conn,$query);
		unlink("data/pics/".mysqli_fetch_assoc($res)['username']);
		//controllo: se è l'ultimo in un gruppo viene eliminato, se era owner deve cambiare
		$query="SELECT id_gruppo FROM appartiene WHERE id_user = '$userId'";
		$res=mysqli_query($conn,$query);
		while ($arr=mysqli_fetch_assoc($res))
		{
			checkUserGroup($conn,$conn_msg,$userId,$arr['id_gruppo']);
		}
		//eliminazione
		$query="DELETE FROM appartiene WHERE id_user = '$userId'";
		mysqli_query($conn,$query);
		$query="DELETE FROM user WHERE id = '$userId'";
		$tmp=mysqli_query($conn,$query);
		if (!$ret)	return;
		echo json_encode($tmp);
	}

?>
