<?php
	function language(){
		echo getLangFile();
	}
	function getLangFile()
	{
		$lang = 'lang/'.basename($_COOKIE['lang']).'.json';
		if(!is_file($lang)) $lang = 'en.json';
		return file_get_contents($lang);
	}
	function getServerText()
	{
		$file=getLangFile();
		$lang=json_decode($file, true);
		return $lang['server'];
	}
?>
