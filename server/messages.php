<?php	
	function sendMessage($conn,$conn_msg,$idgroup,$user,$txt) 
	{
		$query="SELECT pwd FROM gruppi WHERE id = '$idgroup'";
		$res=mysqli_query($conn,$query);
		$key=mysqli_fetch_assoc($res)['pwd'];
		if ($key!='')
			$txt=cipher($txt, $key, "encrypt");
		$tabnm="msg_".$idgroup;
		return mysqli_query($conn_msg,"INSERT INTO $tabnm (txt,id_mittente) VALUES ('$txt','$user')");
	}
	
	function getMessages($conn,$conn_msg,$idgroup,$iduser,$lastId)
	{	
		$query="SELECT ultimo_accesso,data_iscrizione,pwd FROM appartiene JOIN gruppi ON gruppi.id=id_gruppo WHERE id_user ='$iduser' AND id_gruppo = '$idgroup'";
		$res=mysqli_fetch_assoc(mysqli_query($conn,$query));
		$lastAccess=$res['ultimo_accesso'];
		$data_iscrizione=$res['data_iscrizione'];
		$key=$res['pwd'];		
		
		$tabnm="msg_".$idgroup;	
		$query="SELECT * FROM $tabnm WHERE TIMESTAMPDIFF(SECOND,'$data_iscrizione',data) > 0 ";
		
		$res=mysqli_query($conn_msg,$query);
		$ret=array();
		$arr= array();
		$k=0;
		$lastAllRead=0; //id ultimo msg che hanno letto tutti
		while($arr=mysqli_fetch_assoc($res)){
			if (everyoneRead($conn,$idgroup,$arr['data']))	$lastAllRead=$arr['id'];
			if ($arr['id']>$lastId)
			{
				$ret[$k]=array();
				$ret[$k]['user']=getUserName($conn,$arr['id_mittente']);
				$ret[$k]['txt']=$arr['txt'];
				if ($key!='')
					$ret[$k]['txt']=cipher($arr['txt'],$key,"decrypt");
				$ret[$k]['data']=$arr['data'];
				$ret[$k]['id']=$arr['id'];
				if ($lastId==0) //se è la prima volta che prendo i msg, controllo se non li ho letti
				{
					$ret[$k]['newMsg']=!checkData($lastAccess,$arr['data']);
					if ($arr['id_mittente']==$iduser) $ret[$k]['newMsg']=false; //controllo per sicurezza (in teoria non dovrebbe servire)
				}
				$k++;
			}
		}
		$ret[]['lastAllRead']=$lastAllRead;
		return $ret;
	}
	
	function everyoneRead($conn,$groupId,$msgData) //ritorna true se tutti hanno letto il msg
	{
		$query="SELECT id FROM appartiene WHERE id_gruppo='$groupId' AND TIMESTAMPDIFF(SECOND,ultimo_accesso,'$msgData') > 0";
		if (mysqli_fetch_assoc(mysqli_query($conn,$query))['id']=='')	return true;
		return false;
	}
	
	function checkData($data1,$data2)
	{
		//se data 1 più antica di data 2 return false
		if (strtotime($data1) <= strtotime($data2)) return false;
		return true;
	}
	
	function getUserName($conn,$id) //ritorna l'username dell'utente dato il suo id
	{
		$query="SELECT username FROM user WHERE id = '$id'";
		$res=mysqli_query($conn,$query);
		$res=mysqli_fetch_assoc($res)['username'];
		return $res;
	}
	function getLastAccess($conn,$idGroup) //torna elenco ultimo accesso utenti dato idGroup
	{		
		$ret=array();
		$arr=array();
		$k=0;
		$query="SELECT username,last_access FROM user JOIN appartiene ON user.id = id_user WHERE id_gruppo = '$idGroup' ORDER BY last_access DESC,username";
		$res=mysqli_query($conn,$query);
		while($arr=mysqli_fetch_assoc($res)){
			$ret[$k]=array();
			$ret[$k]['user']=$arr['username'];
			$ret[$k]['last_access']=$arr['last_access'];
			$k++;
		}
		return $ret;
	}
	
	function uploadFile(){
		echo json_encode(putFile());
	}
?>
