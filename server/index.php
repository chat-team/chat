<?php
	require "login.php";
	require "messages.php";
	require "online.php";
	require "groups.php";
	require "cipher.php";
	require "account.php";
	require "superuser.php";
	require "mail/mail.php";
	require "data/data.php";
	require "lang/lang.php";
	require "data/global.php"; //contiene variabili globali
	$conn = mysqli_connect($GLOBALS['DB_HOST'],$GLOBALS['DB_USER'],$GLOBALS['DB_PWD'],$GLOBALS['DB_USER']."_chat");
	if ($conn->connect_error)
	{
		echo json_encode("Failed to connect to MySQL: " .$conn->connect_error);
		return;
	}
	
	$conn_msg = mysqli_connect($GLOBALS['DB_HOST'],$GLOBALS['DB_USER'],$GLOBALS['DB_PWD'],$GLOBALS['DB_USER']."_msg");
	if ($conn_msg->connect_error)
	{
		echo json_encode("Failed to connect to MySQL: " .$conn_msg->connect_error);
		return;
	}
		
	$op=$_REQUEST['op'];
	switch ($op)
	{
		case "getOurEmail":
			echo json_encode($GLOBALS['OUR_EMAIL']);
			break;
		case "contactUs":
			echo json_encode(sendMail($GLOBALS['OUR_EMAIL'],$_REQUEST['object'],$_REQUEST['txt']."<br/>".$_REQUEST['email'],$_REQUEST['name']));
			break;
		case "autologin":
			echo json_encode(autologin($conn,$_REQUEST['id'],$_REQUEST['key']));
			break;
		case "login":
			echo json_encode(login($conn,$_REQUEST['user'],$_REQUEST['pwd']));
			break;
		case "register":
			echo json_encode(register($conn,$_REQUEST['user'],$_REQUEST['pwd'],$_REQUEST['name'],$_REQUEST['email']));
			break;
		case "userCheck":
			echo json_encode(userCheck($conn,$_REQUEST['user']));
			break;
		case "emailCheck":
			echo json_encode(emailCheck($conn,$_REQUEST['email']));
			break;
		case "send": 
			echo json_encode(sendMessage($conn,$conn_msg,$_REQUEST['groupId'],$_REQUEST['user'],$_REQUEST['txt']));
			break;
		case "receive": 
			echo json_encode(getMessages($conn,$conn_msg,$_REQUEST['groupId'],$_REQUEST['userId'],$_REQUEST['lastId']));
			break;
		case "uploadFile":
			uploadFile();
			break;
		case "getGroups": 
			echo json_encode(getGroups($conn,$conn_msg,$_REQUEST['idUser']));
			break;
		case "findGroups":
			echo json_encode(findGoups($conn,$_REQUEST['text'],$_REQUEST['idUser']));
			break;
		case "getGroupInfo":
			echo json_encode(getGroupInfo($conn,$_REQUEST['groupId']));
			break;	
		case "createGroup":
			echo json_encode(createNewGroup($conn,$conn_msg,$_REQUEST['userId'],$_REQUEST['name'],$_REQUEST['description'],$_REQUEST['pwd']));
			break;
		case "loginGroup":
			echo json_encode(loginGroup($conn,$_REQUEST['userId'],$_REQUEST['groupId'],$_REQUEST['pwd']));
			break;
		case "exitGroup":
			exitGroup($conn,$conn_msg);
			break;
		case "changeName":
			changeName($conn);
			break;
		case "changePwd":
			changePwd($conn);
			break;
		case "changeAvatar":
			changeAvatar();
			break;
		case "setAccess":
			echo json_encode(setNewLastAccess($conn,$conn_msg,$_REQUEST['userId'],$_REQUEST['groupId'],$_REQUEST['getLast']));
			break;
		case "getAccess":
			echo json_encode(getLastAccess($conn,$_REQUEST['groupId']));
			break;
		case "confirmMail":
			emailConfirmed($conn,$_GET['id'],$_GET['user'],$_GET['string']);
			break;
		case "recoverUser":
			recoverUser($conn);
			break;
		case "recoverPwd":
			recoverPwd($conn);
			break;
		case "confirmPwdRecovery":
			confirmPwdRecovery($conn);
			break;
		case "recoverPwdSubmit":
			recoverPwdSubmit($conn);
			break;		
		case "getMyGroups";
			getGroupsAdmin($conn);
			break;
		case "changeGroupDescription";
			changeGroupDescription($conn);
			break;
		case "deleteUser";
			deleteUser($conn,$conn_msg); 
			break;
		case "put";
			putFile();
			break;
		case "get";
			switch($_REQUEST['type']){
				case "attachment";
					getFile();
					break;
				case "avatar";
					getPic();
					break;
			}
			break;
		//superuser
		case "banUser":
			banUser($conn);
			break;
		case "deleteBan":
			deleteBan($conn);
			break;
		case "checkSU":
			checkSU($conn);
			break;
		case "getAllUsers":
			getAllUsers($conn);
			break;
		case "getAllChat":	
			getAllChat($conn);
			break;
		case "deleteAccount":;
		case "upToSU":	
			upToSU($conn);
			break;

		case "language":
			language();
			break;
		default:
			header('Location: '.$GLOBALS['DOMAIN']);
			break;
	}	
?>
