<?php
	function autologin($conn,$uid,$key){
		$res = mysqli_query($conn,"SELECT * FROM login_key WHERE user_id='$uid' and auth_key='$key'");
		if(!mysqli_num_rows($res)){
			return false;
		}else{
			$val = login_data($uid,$conn);
			if($val['ret'][0] == 'block')
				return false;
			else
				return $val;
		}
	}

	function login($conn,$user,$pwd)
	{			
		$pwd =hashPwd($pwd,$user);
		$res=mysqli_query($conn,"SELECT pwd,id FROM user WHERE username= '$user' and pwd= '$pwd' ");
		$arr=mysqli_fetch_assoc($res);
		if($pwd==$arr['pwd']){
			//cancello una eventuale richiesta di aggiornamento pwd
			$id=$arr['id'];
			mysqli_query($conn,"DELETE FROM recover_pwd WHERE user_id = '$id'");
			return login_data($id,$conn);
		}else{
			$ret = array('ret' => array());
			$ret['ret'][0]=false;
			return $ret;
		}
	}

	function login_data($user,$conn){
		$return = array();
		$ret =array();
		$res=mysqli_query($conn,"SELECT * FROM user WHERE id = '$user'");
		$arr=mysqli_fetch_assoc($res);
		$check=checkBlock($conn,$ret[1]);
		if ($check[0]) //utente bloccato
		{
			$ret[0]='block';
			$return['ret'] = $ret;
			return $return;
		}
		$ret[0]='true';
		$ret[1]=$arr['username'];
		$ret[2]=$arr['name'];
		$ret[3]=$arr['email'];
		$ret[4]=$arr['superuser'];
		$ret[5]=0;
		$id = $arr['id'];
		$res=mysqli_query($conn,"SELECT id FROM gruppi WHERE id_creatore = '$id'");
		while(mysqli_fetch_assoc($res)){
			$ret[5]++;
		}
			
		$key = hash("sha256",strval(rand()));
		mysqli_query($conn,"REPLACE INTO login_key (user_id,auth_key) VALUES ('$id','$key')");
		$return['ret'] = $ret;
		$return['id']  = $id;
		$return['key'] = $key;

		return $return;
	}
	
	
	function register($conn,$user,$pwd,$name,$email)
	{
		$pwd =hashPwd($pwd,$user);	
		//controllo nome utente già utilizzato
		if (userCheck($conn,$user))
			return 'error1';
		//controllo email già utilizzata
		if (emailCheck($conn,$email))
			return 'error2';
		
		//creo avatar --> spostata in mail.php (creiamo l'avatar quando l'utente conferma l'account)
		
		//query di inserimento
		//calcolo stringa casuale
		$string=hash("sha256",$user.strval(rand(0,2147483647)).$pwd);
		
		$res=mysqli_query($conn,"INSERT INTO user_prov (username,name,pwd,email,check_string) VALUES ('$user','$name','$pwd','$email','$string')");
		
		$tmp=mysqli_query($conn,"SELECT id FROM user_prov WHERE username='$user'");
		$id=mysqli_fetch_assoc($tmp)['id'];
		
		//invio email
		
		$lang=getServerText();
		
		$link=$GLOBALS['REAL_DOMAIN']."/server/index.php?op=confirmMail&id=$id&user=$user&string=$string";
		$subject=$lang['confirmation'];
		$body=$lang['hello']." ".$user.", ".$lang['text1'].".<br/>".$lang['text2'].":<br/>".$link."<br/>".$lang['team'];
		
		if (!(sendMail($email, $subject, $body)))
			return 'error3';
		
		return true;
	}

	function userCheck($conn,$user)
	{
		$res= mysqli_query($conn,"SELECT username FROM user WHERE username= '$user' UNION SELECT username FROM user_prov WHERE username= '$user' ");
		if(mysqli_fetch_assoc($res)){
			$res=mysqli_query($conn,"SELECT username FROM user WHERE username LIKE '".$user."_%' UNION SELECT username FROM user_prov WHERE username LIKE '".$user."_%'");
			while($arr[] = mysqli_fetch_assoc($res)['username']);
			$i=0;
			while(in_array($user.'_'.$i,$arr))
				$i++;
			return $user.'_'.$i;
		}
		return false;
	}

	function emailCheck($conn,$email)
	{
		$res= mysqli_query($conn,"SELECT email FROM user WHERE email= '$email' UNION SELECT email FROM user_prov WHERE email= '$email' ");
		if(mysqli_fetch_assoc($res))
			return true;
		return false;
	}

	function checkBlock($conn,$userId)	//ritorna true se è bloccato, false se è libero + data di sblocco
	{
		$query = "SELECT data_sblocco FROM blocco_user WHERE id_user = '$userId'";
		$res = mysqli_query($conn,$query);
		$data=mysqli_fetch_assoc($res)['data_sblocco'];
		$ret=array();
		$ret[0]=false;
		$ret[1]='';
		if($data=='')
			return $ret;
		else
		{
			//comparo le due date
			if (checkData($data,date("Y-m-d H:i:s", time())))
			{ 
				$ret[0]=true;
				$ret[1]=$data;
				return $ret;	//blocco non terminato
			}
			else	//blocco terminato, elimino blocco
			{
				$query = "DELETE FROM blocco_user WHERE id_user = '$userId'";
				mysqli_query($conn,$query);
				return $ret;
			}
		}
	}
	//recupero pwd e username
	function recoverUser($conn)
	{
		$email=$_REQUEST['email'];
		$query="SELECT username FROM user WHERE email='$email'";
		$res=mysqli_query($conn,$query);
		$user=mysqli_fetch_assoc($res)['username'];
		if($user=='')
		{
			echo json_encode(false);
			return;
		}
		
		//invio email
		
		$lang=getServerText();
		
		$subject=$lang['recover-user'];
		$body=$lang['hello'].", ".$lang['text3'].": <b>".$user."</b>.<br/>".$lang['team'];
		
		if (!(sendMail($email, $subject, $body)))
		{
			echo json_encode('error');
			return;
		}
		echo json_encode(true);
	}
	
	function recoverPwd($conn)
	{
		$user=$_REQUEST['user'];
		$query="SELECT id,pwd,email FROM user WHERE username='$user'";
		$res=mysqli_query($conn,$query);
		$arr=mysqli_fetch_assoc($res);
		$id=$arr['id'];
		if($id=='')
		{
			echo json_encode(false);
			return;
		}
		$pwd=$arr['pwd'];
		$email=$arr['email'];
		
		//query inserimento/updating
		//calcolo stringa casuale
		$string=hash("sha256",$user.strval(rand(0,2147483647)).$pwd);
		$res=mysqli_query($conn,"SELECT user_id FROM recover_pwd WHERE user_id='$id'");
		if (mysqli_fetch_assoc($res)) //update
			mysqli_query($conn,"UPDATE recover_pwd SET check_string='$string' WHERE user_id='$id'");
		else 	//insert	
			mysqli_query($conn,"INSERT INTO recover_pwd (user_id,check_string) VALUES ('$id','$string')");
		
		//invio email
		$lang=getServerText();
		
		$subject=$lang['recover-pwd'];
		$link=$GLOBALS['REAL_DOMAIN']."/server/index.php?op=confirmPwdRecovery&id=$id&string=$string";
		$body=$lang['hello']." ".$user.", ".$lang['text4'].":<br/>".$link."<br/>".$lang['team'];
		if (!(sendMail($email, $subject, $body)))
		{
			echo json_encode('error');
			return;
		}
		echo json_encode(true);
	}
	
	function recoverPwdSubmit($conn)
	{
		$userId=$_REQUEST['userId'];
		$user=$_REQUEST['user'];
		$pwd=hashPwd($_REQUEST['pwd'],$user);
		$string=$_REQUEST['string'];
		$query="SELECT pwd FROM user 
			JOIN recover_pwd ON id=user_id 
			WHERE username='$user' AND user_id='$userId' AND check_string='$string' ";
		$oldPwd=mysqli_fetch_assoc(mysqli_query($conn,$query))['pwd'];
		if ($oldPwd!='')
		{
			mysqli_query($conn,"DELETE FROM recover_pwd WHERE user_id = '$userId'");
			mysqli_query($conn,"UPDATE user SET pwd = '$pwd' WHERE id = '$userId'");
			// aggiorno avatar
			$pic = cipher(file_get_contents('data/pics/'.$user),$oldPwd,'decrypt');
			file_put_contents('data/pics/'.$user,cipher($pic,$pwd,'encrypt'));
			echo json_encode(true);
		}
		else
			echo json_encode(false); 
	}
?>
