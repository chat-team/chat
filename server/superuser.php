<?php

	function checkSU($conn)  //check credenziali SU
	{
		$username=$_REQUEST['user'];
		$id=$_REQUEST['id'];
		$pwd=hashPwd($_REQUEST['pwd'],$username);
		$query="SELECT pwd,superuser FROM user WHERE username = '$username' AND id = '$id'";
		$res=mysqli_fetch_assoc(mysqli_query($conn,$query));
		if ($res['pwd']==$pwd && $res['superuser']==1)
			echo json_encode(true);
		else
			echo json_encode(false);
	}
	
	function getAllUsers($conn)
	{
		$query="SELECT * FROM user";
		$res=mysqli_query($conn,$query);
		$ret=array();
		$arr=array();
		$k=0;
		while ($arr=mysqli_fetch_assoc($res))
		{
			$ret[$k]=array();
			$ret[$k]['id']=$arr['id'];
			$ret[$k]['user']=$arr['username'];
			$ret[$k]['name']=$arr['name'];
			$ret[$k]['email']=$arr['email'];
			$ret[$k]['SU']=$arr['superuser'];
			$ret[$k]['ultimo_accesso']=$arr['last_access'];
			$check=checkBlock($conn,$arr['id']);
			$ret[$k]['block']=$check[0];
			$ret[$k]['fine_blocco']=$check[1];	
			$k++;
		}
		echo json_encode($ret);
	}
	
	function getAllChat($conn)
	{
		$query="SELECT id,nome,privata,descrizione,id_creatore FROM gruppi";
		$res=mysqli_query($conn,$query);
		$ret=array();
		$arr=array();
		$k=0;
		while ($arr=mysqli_fetch_assoc($res))
		{
			$ret[$k]=array();
			$ret[$k]['id']=$arr['id'];
			$ret[$k]['nome']=$arr['nome'];
			$ret[$k]['descrizione']=$arr['descrizione'];
			$ret[$k]['privata']=$arr['privata'];
			$ret[$k]['owner']=getUserName($conn,$arr['id_creatore']);	
			$k++;
		}
		echo json_encode($ret);
	}
	
	function banUser($conn)
	{
		$idUser = $_REQUEST['userId'];
		$blockDate = $_REQUEST['banDate'];
		$query = "INSERT INTO blocco_user (id_user, data_sblocco) VALUES ('$idUser', '$blockDate')";
		mysqli_query($conn,$query);

		//recupero nome e mail dell'user, prima controllo che non sia SU
		$query = "SELECT email, username, superuser FROM user WHERE id = '$idUser'";
		$arr = mysqli_fetch_assoc(mysqli_query($conn,$query));
		$superuser = $arr['superuser'];
		if($superuser==0){
			$email = $arr['email'];
			$nome = $arr['username'];

			//invio email
			$subject = "Sospensione account utente ChatPhi";
			$body = "Ciao ".$nome.",<br>Siamo spiacenti di comunicarti che il tuo account è stato bloccato dall'admin fino alla data ".$blockDate.".";

			if (!(sendMail($email, $subject, $body)))
			{
				echo json_encode('error');
				return;
			}
			echo json_encode(true);
		}else echo json_encode(false);
	}

	function mailDeleteUserSU($conn, $idUser) //NON ID SUPERUSER ma id user da cancellare
	{
		//recupero nome e mail dell'user
		$query = "SELECT email, username FROM user WHERE id = '$idUser'";
		$arr = mysqli_fetch_assoc(mysqli_query($conn,$query));
		$email = $arr['email'];
		$nome = $arr['username'];
		//invio email
		$subject = "Eliminazione account utente ChatPhi";
		$body = "Ciao ".$nome.",<br>Siamo spiacenti di comunicarti che il tuo account è stato eliminato permanentemente dall'admin.";
		if (!(sendMail($email, $subject, $body)))
		{
			echo json_encode('error');
			return false;
		}
		return true;
	}

	function upToSU($conn)
	{
		$idUser = $_REQUEST['userId'];
		$query = "SELECT email, username FROM user WHERE id = '$idUser'";
		$arr = mysqli_fetch_assoc(mysqli_query($conn,$query));
		$email = $arr['email'];
		$nome = $arr['username'];
		//invio email
		$subject = "Sei SuperUser ChatPhi";
		$body = "Ciao ".$nome.",<br>Good Job!!!!! Sei stato promosso a SuperUser dall'admin.";
		if (!(sendMail($email, $subject, $body)))
		{
			echo json_encode('error');
		}
	
		echo json_encode(mysqli_query($conn,"UPDATE user SET superuser = 1 WHERE id = '$idUser'"));
	}
	
	function downToUser($conn)
	{
		$idUser = $_REQUEST['userId'];
		$query = "SELECT email, username FROM user WHERE id = '$idUser'";
		$arr = mysqli_fetch_assoc(mysqli_query($conn,$query));
		$email = $arr['email'];
		$nome = $arr['username'];
		//invio email
		$subject = "Sei stato declassato a user";
		$body = "Ciao ".$nome.",<br>Sei stato declassato a user dall'admin.";
		if (!(sendMail($email, $subject, $body)))
		{
			echo json_encode('error');
		}
		echo json_encode(mysqli_query($conn,"UPDATE user SET superuser = 0 WHERE id = '$idUser'"));
	}

	function sendGenericMail(){
		$email = $_REQUEST['email'];
		$subject =$_REQUEST['subject'];
		$body = $_REQUEST['body'];
		if (!(sendMail($email, $subject, $body)))
		{
			echo json_encode('error');
		}
	}
	
	function deleteUserTemp($conn){
		$data2 = $_REQUEST['data'];
		$query = "SELECT id,data_invio FROM user_prov";
		$res = mysqli_query($conn,$query);
		$arr=array();
		while($arr=mysqli_fetch_assoc($res)){
			if(!checkData($arr['data_invio'],data2))
				$idUser=$arr['id'];
				$query = "DELETE FROM blocco_user WHERE id_user = '$idUser'";
				echo json_encode(mysqli_query($conn,$query));
		}
	}
	
	
	function deleteBan($conn)
	{
		$idUser = $_REQUEST['userId'];
		$query = "SELECT email, username FROM user WHERE id = '$idUser'";
		$arr = mysqli_fetch_assoc(mysqli_query($conn,$query));
		$email = $arr['email'];
		$nome = $arr['username'];
		$query = "DELETE FROM blocco_user WHERE id_user = '$idUser'";
		$subject = "Riabilitazione account utente ChatPhi";
		$body = "Ciao ".$nome.",<br>Siamo felici di comunicarti che il tuo account è stato riabilitato dall'admin.";
		echo json_encode(mysqli_query($conn,$query));
		if (!(sendMail($email, $subject, $body)))
		{
			echo json_encode('error');
		}
		
	}

?>
