<?php

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	require 'PHPMailer/src/Exception.php';
	require 'PHPMailer/src/PHPMailer.php';
	require 'PHPMailer/src/SMTP.php';
	
	function sendMail($to, $subject, $body, $from_name = "ChatPhi") 
	{
		$from=$GLOBALS['OUR_EMAIL'];
		$mail = new PHPMailer();  // creiamo l'oggetto
		$mail->IsSMTP(); // abilitiamo l'SMTP
		$mail->SMTPDebug = 0;  // debug: 1 = solo messaggi, 2 = errori e messaggi
		$mail->SMTPAuth = true;  // abilitiamo l'autenticazione
		$mail->SMTPSecure = 'ssl'; // abilitiamo il protocollo ssl richiesto per Gmail
		$mail->Host = 'smtp.gmail.com'; // ecco il server smtp di google
		$mail->Port = 465; // la porta che dobbiamo utilizzare
		$mail->Username = $GLOBALS['OUR_EMAIL']; //email del nostro account gmail
		$mail->Password = $GLOBALS['OUR_EMAIL_PWD']; //password del nostro account gmail
		$mail->SetFrom($from, $from_name);
		$mail->Subject = $subject;
		$mail->Body = $body;
		$mail->IsHTML(true);
		$mail->AddAddress($to);
		return $mail->Send(); 
	}
	
	function emailConfirmed($conn,$id,$user,$string)
	{
		//check credenziali
		$res= mysqli_fetch_assoc(mysqli_query($conn,"SELECT * FROM user_prov WHERE id= '$id' "));
		$res1=mysqli_fetch_assoc(mysqli_query($conn,"SELECT id FROM user_prov WHERE username= '$user' "));
		if ($res && $res1 && $res['id']==$res1['id'] && $res['check_string']==$string)
		{
			//aggiornamento DB
			$name=$res['name'];
			$mail=$res['email'];
			$pwd=$res['pwd'];
			
			mysqli_query($conn,"INSERT INTO user (username,name,pwd,email,superuser) VALUES ('$user','$name','$pwd','$mail','0')");
			mysqli_query($conn,"DELETE  FROM user_prov WHERE id = '$id'");
			
			//creo avatar
			$pic = file_get_contents("https://www.gravatar.com/avatar/".md5($user)."?d=identicon&s=200"); 
			putPic($user,base64_encode($pic));
			
			//set cookie
			echo "	<script src='../client/script/gest_cookie.js'></script>
					<script>setRealCookie('loginOption','account_created')</script>	
			";
			//redirect  
			echo "<script>window.location.href='".$GLOBALS['DOMAIN']."'</script>";
		}
		else
		{
			echo "
				<head>
					<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css'>
					<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
					<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js'></script>
					<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js'></script>
		
					<script src='../client/script/lang.js'></script>
					<script src='../client/script/gest_cookie.js'></script>
				</head>
				
				<div class='dropdown' style='position:absolute;top:2px;right:10px'>
					<button type='button' class='btn btn-light dropdown-toggle' data-toggle='dropdown'>
						<span translation='language' >Scegli lingua</span>
					</button>
					<div class='dropdown-menu'>
						<a class='dropdown-item' onclick='cambia_lingua(";echo '"it"'; echo ")'>Italiano</a>
						<a class='dropdown-item' onclick='cambia_lingua(";echo '"en"'; echo ")'>English</a>
					</div>
				</div>

				<div class='jumbotron container-fluid'>
					<div class='container'>
						<span class='display-4'>ChatPhi</span><i style='font-size:40px'>&nbsp;-&nbsp;<span translation='title'>Il nuovo sistema di messaggistica</span></i>
					</div>
				</div>
				
				<div class='container'>
					<h1 translation='mail-error1' >Parametri non validi o utente già registrato</h1><br/>
					<h3 translation='mail-error2' >Se il problema persiste contattare l'assistenza di ChatPhi</h3><br/>
					<a translation='mail-error3' href=".$GLOBALS['DOMAIN'].">Torna alla home</a>
				</div>
			";	
		}	
	}
	
	function confirmPwdRecovery($conn)
	{
		$id=$_REQUEST['id'];
		$string=$_REQUEST['string'];
		echo "
			<head>
				<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css'>
				<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
				<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js'></script>
				<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js'></script>
	
				<script src='../client/script/lang.js'></script>
				<script src='../client/script/gest_cookie.js'></script>
				<script src='../client/script/recover_pwd.js'></script>
				<script src='../client/script/gest_notifiche.js'></script>
				<script src='../client/libs/notify.min.js'></script>
				<script src='../client/libs/password.min.js'></script>
				
				<link type='text/css' rel='stylesheet' href='../client/libs/password.min.css' />
				<link rel='stylesheet' type='text/css' href='../client/css/newStyle.css' />
			</head>
			<body>
			<div class='dropdown' style='position:absolute;top:2px;right:10px' >
				<button type='button' class='btn btn-light dropdown-toggle' data-toggle='dropdown'>
					<span translation='language' >Scegli lingua</span>
				</button>
				<div class='dropdown-menu'>
					<a class='dropdown-item' onclick='cambia_lingua(";echo '"it"'; echo ")'>Italiano</a>
					<a class='dropdown-item' onclick='cambia_lingua(";echo '"en"'; echo ")'>English</a>
				</div>
			</div>

			<div class='jumbotron container-fluid'>
				<div class='container'>
					<span class='display-4'>ChatPhi</span><i style='font-size:40px'>&nbsp;-&nbsp;<span translation='title'>Il nuovo sistema di messaggistica</span></i>
				</div>
			</div>";
			
			//check credenziali
			
			$query="SELECT check_string FROM recover_pwd WHERE user_id='$id'";	
			$res=mysqli_fetch_assoc(mysqli_query($conn,$query));
			if ($res && $res['check_string']==$string)
			{
				echo "
				<div class='container' onkeydown='checkInvio(event,$id,";echo '"';echo $string;echo '"'; echo")'>
					<form id='restorePwdForm' style='display:block;max-width:600px;'>
						<h3 translation='recover-pwd'>Recupera password</h3>
						
						<div class='form-group'>
							<label for='user'><span translation='username'>Username</span>:</label>
							<input type='text' id='user' translation='username' placeholder='Inserire username&hellip;'  maxlength='50' class='form-control'>
						</div>
						<div class='form-group'>
							<label for='pwd1'><span translation='new-password'>Nuova password</span>:</label>
							<input type='password' id='pwd1' translation='password' feature='pwd-strength' placeholder='Inserire password...' class='form-control'>
						</div>
						<div class='form-group'>
							<label for='pwd2'><span translation='confirm-password'>Conferma password</span>:</label>
							<input type='password' id='pwd2' translation='confirm-pwd' placeholder='Confermare password...' class='form-control'>
						</div>
						<input type='button' id='submit' translation='confirm' value='Conferma' class='btn btn-primary btn-block' onclick='recoverPwdSubmit($id,";echo '"';echo $string;echo '"'; echo")' />
						<br/><a translation='mail-error3' href=".$GLOBALS['DOMAIN'].">Torna alla home</a>
					</form>
				</div>
				";
			}
			else
			{
				echo "
				<div class='container'>
					<h1 translation='mail-error4' >Parametri non validi o password già aggiornata</h1><br/>
					<h3 translation='mail-error2' >Se il problema persiste contattare l'assistenza di ChatPhi</h3><br/>
					<a translation='mail-error3' href=".$GLOBALS['DOMAIN'].">Torna alla home</a>
				</div>";
			}
			echo "</body>";
	}
?>
