var chatList=new Array(); //contiene elenco chat ordinato in base all'ultimo msg ricevuto
var FIND_CHAT;//serve per annullare la ricerca della chat se dopo poco tempo cambia il testo

function getGroups()
{
	document.getElementById('search').value='';
	gestResize();
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			op: "getGroups",
			idUser: getCookie("auth_id")  
		} ,
		success: function(response){
			var data=JSON.parse(response);
			console.log(data);
			if (data.length==0)
				document.getElementById('groupList').innerHTML='<span class="text-secondary">'+$translation['no-chat']+'</span>';
			var tot=0;
			for (var k=0;k<data.length;k++)
			{
				chatList[k]=data[k];
				tot+=parseInt(data[k]['new_msg']);	
			}
			//riordino chatList
			chatSort();
			for (var k=0;k<chatList.length;k++)
				insertGroup(chatList[k]);
			checkLastMsg();
			checkBlue();
			//nuovi msg totali
			$('#totNewMsg').html((tot==0)?'':tot);
		} ,
		error: function(response){
			setNotify('menu',$translation['server-offline'],'error','buttom center');
		}
	});
}

function checkBlue() //check spunte blu
{
	for (var k=0;k<chatList.length;k++)
	{
		if (chatList[k]['last_all_read'] && chatList[k]['last_sender']==getTempCookie('loginData1'))
			document.getElementById('lastRead_'+chatList[k]['id']).setAttribute('src','img/msgRead.png');
	}
}

function checkLastMsg() //controlla il display ultimo msg (es. se è un' immagine non va visualizzata)
{
	var chat=document.getElementsByClassName('chat_el');
	var img,a;
	for (var k=0;k<chat.length;k++)
	{
		//check img
		img=chat[k].getElementsByTagName('img');
		if (img[0]!=undefined)
		{
			var modify=false;
			if (img[1]!=undefined) 
			{
				img=img[1];
				modify=true;
			}
			else if (img[0].id.split('_')[0]!='lastRead')
			{
				img=img[0];
				modify=true;
			}
			if (modify)
			{
				img.setAttribute('style','display:none');
				var photo=document.createElement('img');
				photo.setAttribute('src','img/picture.png');
				photo.setAttribute('style','width:20px;height:20px');
				chat[k].appendChild(photo);
				chat[k].innerHTML+='&nbsp;'+$translation['photo'];
			}
		}
		//check attach
		a=chat[k].getElementsByTagName('a')[0];
		if (a!=undefined)
		{
			a.setAttribute('style','display:none');
			var photo=document.createElement('img');
			photo.setAttribute('src','img/attach.png');
			photo.setAttribute('style','width:20px;height:20px');
			chat[k].appendChild(photo);
			chat[k].innerHTML+='&nbsp;'+a.innerHTML;
		}
	}
}

function gestResize()  //setta posizione addButton, altezza groupContainer e larghezza chat
{
	var menu=document.getElementById('menu').offsetHeight;
	var button=document.getElementById('addButton');
	var container=document.getElementById('groupList');
	container.style.height=(window.innerHeight-menu)+'px';
	button.style.top=window.innerHeight-button.offsetHeight-5+"px";
	button.style.right=parseInt(document.body.clientWidth/2-container.clientWidth/2)+5+"px";
	//resize tr delle chat
	var id,img,ora;
	for (var k=0;k<chatList.length;k++)
	{
		try{
			id='c_'+chatList[k]['id']+'_';
			img=document.getElementById(id+'1').offsetWidth;
			ora=document.getElementById(id+'3').offsetWidth;
			document.getElementById(id+'2').style.width=container.offsetWidth-img-ora-5+'px';
		}catch(e){};
	}
}

function insertGroup(chat)
{
	var pathImg='https://images.everyeye.it/img-notizie/crash-bandicoot-sony-pubblica-un-immagine-twitter-v2-258919-1280x720.jpg';
	
	var tbody=document.getElementById('tableGroup');
	var tr=document.createElement('tr');
	tr.setAttribute('onclick','sendGroupData("'+chat['name']+'",'+chat['id']+')');
	var td=document.createElement('td');
	td.setAttribute('id','c_'+chat['id']+'_1');
	var img=document.createElement('img');
	img.setAttribute('class','rounded-circle');
	img.setAttribute('style','height:50px;width:50px');
	img.setAttribute('src',pathImg);
	td.setAttribute('style','padding:5px;width:60px');
	td.appendChild(img);
	tr.appendChild(td);
	var td=document.createElement('td');
	var name=document.createElement('span');
	name.innerHTML=chat['name'];
	name.setAttribute('class','chatName');
	td.appendChild(name);
	if (chat['pwd'])
	{
		var img=document.createElement('img');
		img.setAttribute('src','img/close.png');
		img.setAttribute('style','width:13px;height:13px');
		td.innerHTML+='&nbsp;'
		td.appendChild(img);
	}
	if (chat['last_msg'])
	{
		var sender=document.createElement('div');
		if (chat['last_sender']==getTempCookie("loginData1"))
		{
			var read=document.createElement('img');
			read.setAttribute('src','img/msgSend.png');
			read.setAttribute('style','width:20px;height:20px');
			read.setAttribute('id','lastRead_'+chat['id']);
			sender.appendChild(read);
		}
		else
			sender.innerHTML=chat['last_sender']+":";
		sender.innerHTML+=" "+chat['last_msg'];
		sender.setAttribute('class','text-secondary chat_el');
		sender.setAttribute('style','width:100px;overflow:hidden');
		sender.setAttribute('id','c_'+chat['id']+'_2');
		td.setAttribute('style','border-bottom:1px solid #ADADAD;white-space: nowrap;');
		td.appendChild(sender);
	}
	tr.appendChild(td);
	var td=document.createElement('td');
	td.setAttribute('class','text-center');
	td.setAttribute('id','c_'+chat['id']+'_3');
	if (chat['last_msg'])
	{
		var ora=document.createElement('span');
		ora.setAttribute('class','text-secondary');
		
		var newMsg=document.createElement('span');
		newMsg.setAttribute('class','badge badge-success');
		newMsg.setAttribute('id','newMsg_'+chat['id']);
		if (chat['new_msg'])
		{
			newMsg.innerHTML=chat['new_msg'];
			ora.setAttribute('class','text-success');
			sender.setAttribute('class','text-success');
		}			
		td.appendChild(newMsg);
		td.appendChild(document.createElement('br'));
		
		var tmp=getDateTxt(chat['last_msg_time'],false);
		if (tmp==$translation['today'])	tmp=getDateTxt(chat['last_msg_time'],true);
		ora.innerHTML=tmp; 
		
		td.appendChild(ora);
	}
	tr.appendChild(td);
	tbody.appendChild(tr);

	gestResize();
}

function sendGroupData(name,id)
{
	setTempCookie('groupName',name);
	setTempCookie('groupId',id);
	window.location.href='messaggi.html';
}

function findChat()
{
	document.getElementById('chatFound').innerHTML='';
	var input=document.getElementById('search').value;
	if (input=='')
		document.getElementById('no-chat').style.display='none';
	else
	{ 
		clearTimeout(FIND_CHAT);
		FIND_CHAT=setTimeout(function(){
			$.ajax({
				url:'../server/index.php',
				type: 'POST',
				data: { 
					op: "findGroups",
					text: input,
					idUser: getCookie("auth_id")
				} ,
				success: function(response){
					var data=JSON.parse(response);
					if (data.length>0)
					{
						document.getElementById('chatFound').innerHTML='';	
						document.getElementById('no-chat').style.display='none';
						for (var k=0;k<data.length;k++)
							insertFoundGroup(data[k]);
					}
					else
						document.getElementById('no-chat').style.display='block';
				} ,
				error: function(response){
					setNotify('newChatNotify',$translation['server-offline'],'error','top center');
				}
			});
		},200); 
	}
}

function insertFoundGroup(chat)
{
	var container=document.getElementById('chatFound');
	var a=document.createElement('a');
	a.setAttribute('class','list-group-item list-group-item-action chatName');
	a.setAttribute('href','#');
	a.setAttribute('onclick','openFoundChat('+chat['id']+')');	
	a.innerHTML=chat['name'];
	if (chat['pwd'])
	{
		var img=document.createElement('img');
		img.setAttribute('src','img/close.png');
		img.setAttribute('style','width:13px;height:13px');
		a.innerHTML+='&nbsp;'
		a.appendChild(img);
	}
	container.appendChild(a);
}

function openFoundChat(id)
{
	document.getElementById('backNormal').style.display='inline';
	document.getElementById('chat_details').style.display='block';
	document.getElementById('normal_body').style.display='none';
	document.getElementById('confirmButton').style.display='block';
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			op: "getGroupInfo",
			groupId: id
		} ,
		success: function(response){
			var data=JSON.parse(response);
			if(data)
			{
				var pathImg='https://images.everyeye.it/img-notizie/crash-bandicoot-sony-pubblica-un-immagine-twitter-v2-258919-1280x720.jpg';
				
				document.getElementById('chat_pic').setAttribute('src',pathImg);
				$('#chat_name').html(data['name']);
				$('#description').html(data['description']);
				$('#members').html(data['members']);
				$('#owner').html(data['owner']);
				if (data['pwd']) document.getElementById('pwd_required').style.display='block';
				else document.getElementById('pwd_required').style.display='none';
				document.getElementById('confirmButton').setAttribute('onclick','loginGroup('+id+','+data['pwd']+',"'+data['name']+'")');
			}
			else
				setNotify('newChatNotify',$translation['server-offline'],'error','top center');
		} ,
		error: function(response){
			setNotify('newChatNotify',$translation['server-offline'],'error','top center');
		}
	});
}

function backNormal()
{
	document.getElementById('backNormal').style.display='none';
	document.getElementById('chat_details').style.display='none';
	document.getElementById('normal_body').style.display='block';
	document.getElementById('confirmButton').style.display='none';
	document.getElementById('confirmButton').setAttribute('onclick','createNewGroup()');
}

function loginGroup(idGroup,pwd,groupName,op=true)
{
	if (op)
	{
		if (pwd)
		{
			var tmp=$('#pwd').val();
			if (tmp=='')
			{
				setNotify('pwd',$translation['insert-password'],'error','top center');
				return;
			}
			pwd=tmp;
		}
		else
			pwd='';
	}
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			op: "loginGroup",
			userId: getCookie("auth_id"),
			groupId: idGroup,
			pwd: pwd
		} ,
		success: function(response){
			var data=JSON.parse(response);
			if (data=='error')
				setNotify('pwd',$translation['wrong-password'],'error','top center');
			else
			{
				setTempCookie('groupName',groupName);
				setTempCookie('groupId',idGroup);
				window.location.href='messaggi.html';
			}
		} ,
		error: function(response){
			setNotify('newChatNotify',$translation['server-offline'],'error','top center');
		}
	});
}

function createNewGroup()
{
	var name=document.getElementById('newGroupName').value;
	var desc=document.getElementById('newGroupDescr').value;
	var pwd=document.getElementById('newGroupPwd').value;
	var pwd2=document.getElementById('newGroupPwd2').value;
	var op=document.getElementById('newCheck').checked; //true=gruppo privato
	var ret=false;
	if (name=="")
	{
		setNotify('newGroupName',$translation['insert-name'],'error','top center');
		ret=true;
	}
	if (desc=='')
	{
		setNotify('newGroupDescr',$translation['insert-description'],'error','top center');
		ret=true;
	}
	if (pwd=="" && op)
	{
		setNotify('newGroupPwd',$translation['insert-password'],'error','top center');
		ret=true;
	}
	if (pwd2=="" && op)
	{
		setNotify('newGroupPwd2',$translation['confirm-password'],'error','top center');
		ret=true;
	}
	if (ret)	return;
	if (pwd!=pwd2)
	{
		setNotify('newGroupPwd',$translation['pwd-differs'],'error','top center');
		setNotify('newGroupPwd2',$translation['pwd-differs'],'error','top center');
		return;
	}
	
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			op: "createGroup",
			name: name,
			description: desc,
			userId: getCookie("auth_id"),
			pwd: pwd
		} ,
		success: function(response){
			var data=JSON.parse(response);
			if (data)
				loginGroup(data,pwd,name,false);
			else
				setNotify('newChatNotify',$translation['server-offline'],'error','top center');
		} ,
		error: function(response){
			setNotify('newChatNotify',$translation['server-offline'],'error','top center');
		}
	});
}

function checkInvio(event)
{
	var ev=event.keyCode;
}

function changeOptMenu(op)
{
	if (op) //inserisci nuova chat
	{
		document.getElementById('addChatMenu0').setAttribute('class','nav-link');
		document.getElementById('addChatMenu1').setAttribute('class','nav-link active');	
		document.getElementById('findChat').style.display='none';
		document.getElementById('addChat').style.display='block';
		document.getElementById('confirmButton').style.display='block';
	}
	else
	{
		document.getElementById('addChatMenu1').setAttribute('class','nav-link');
		document.getElementById('addChatMenu0').setAttribute('class','nav-link active');
		document.getElementById('findChat').style.display='block';
		document.getElementById('addChat').style.display='none';
		document.getElementById('confirmButton').style.display='none';
	}
}

function newCheckClicked() //cambiamento chat privata/pubblica
{
	if (document.getElementById('newCheck').checked)
		document.getElementById('privateChat').style.display='block';
	else
		document.getElementById('privateChat').style.display='none';
}

function newCheckClicked2()
{
	if (document.getElementById('newCheck').checked)
		document.getElementById('newCheck').checked=false;
	else
		document.getElementById('newCheck').checked=true;
	newCheckClicked();
}

function clearInput()
{
	document.getElementById('no-chat').style.display='none';
	document.getElementById('newGroupPwd').value='';
	document.getElementById('newGroupPwd2').value='';
	document.getElementById('newCheck').checked=false;
	document.getElementById('privateChat').style.display='none';
	$('#newGroupPwd').keyup();
}
