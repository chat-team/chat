//servono a evitare che la notifica nuovi msg continui ad apparire se l'utente l'ha chiusa e non ci sono nuovi msg
var FIRST_TIME=true;
var gestNotify=new Array();

/*
 * id   => id dell'oggetto sul quale far apparire la notifica
 * txt  => testo della notifica
 * type => tipo di notifica (success, info, warn, error)
 * pos  => posizione di comparsa rispetto all'oggetto. altezza:(top, middle, bottom) larghezza:(left, center, right)
 * CAMPI OPZIONALI:
 * hide => chiudere la notifica se cliccata (default:true) 
 * auto => chiusura automatica dopo n millisecondi (default:true)
 * mSec => n millisecondi (default:5000)
 * aTmp => tempo di apertura animazione (default:400)
*/
function setNotify(id,txt,type,pos,hide=true,auto=true,mSec=5000,aTmp=400,cTmp=200)
{	
	$('#'+id).notify(txt,{ 
		clickToHide: hide,
		autoHide: auto,
		autoHideDelay: mSec,
		arrowShow: true,
		arrowSize: 5,
		position: pos,
		elementPosition: 'bottom left',
		globalPosition: 'top right',
		style: 'bootstrap',
		className: type,
		showAnimation: 'slideDown',
		showDuration: aTmp,
		hideAnimation: 'slideUp',
		hideDuration: cTmp,
		gap: 2
	});
}

/*
 * nMsg     => numero di nuovi msg  
 * nChat    => numero di chat con nuovi msg
 * SE nChat==1 SI PASSA ANCHE:
 * nomeChat => nome della chat 
 * idChat   => id della chat
 */
function setMsgNotify(nMsg,nChat,nomeChat='',idChat=0) //setta notifiche nuovo messaggio
{
	$.notify.addStyle('newMsg', {
		html: 
		"<div id='newMsg' style='height:300px'>" +
			"<div id='clearfix'>" +
				"<div id='title'><img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QYFAhkSsdes/QAAA8dJREFUOMvVlGtMW2UYx//POaWHXg6lLaW0ypAtw1UCgbniNOLcVOLmAjHZolOYlxmTGXVZdAnRfXQm+7SoU4mXaOaiZsEpC9FkiQs6Z6bdCnNYruM6KNBw6YWewzl9z+sHImEWv+vz7XmT95f/+3/+7wP814v+efDOV3/SoX3lHAA+6ODeUFfMfjOWMADgdk+eEKz0pF7aQdMAcOKLLjrcVMVX3xdWN29/GhYP7SvnP0cWfS8caSkfHZsPE9Fgnt02JNutQ0QYHB2dDz9/pKX8QjjuO9xUxd/66HdxTeCHZ3rojQObGQBcuNjfplkD3b19Y/6MrimSaKgSMmpGU5WevmE/swa6Oy73tQHA0Rdr2Mmv/6A1n9w9suQ7097Z9lM4FlTgTDrzZTu4StXVfpiI48rVcUDM5cmEksrFnHxfpTtU/3BFQzCQF/2bYVoNbH7zmItbSoMj40JSzmMyX5qDvriA7QdrIIpA+3cdsMpu0nXI8cV0MtKXCPZev+gCEM1S2NHPvWfP/hL+7FSr3+0p5RBEyhEN5JCKYr8XnASMT0xBNyzQGQeI8fjsGD39RMPk7se2bd5ZtTyoFYXftF6y37gx7NeUtJJOTFlAHDZLDuILU3j3+H5oOrD3yWbIztugaAzgnBKJuBLpGfQrS8wO4FZgV+c1IxaLgWVU0tMLEETCos4xMzEIv9cJXQcyagIwigDGwJgOAtHAwAhisQUjy0ORGERiELgG4iakkzo4MYAxcM5hAMi1WWG1yYCJIcMUaBkVRLdGeSU2995TLWzcUAzONJ7J6FBVBYIggMzmFbvdBV44Corg8vjhzC+EJEl8U1kJtgYrhCzgc/vvTwXKSib1paRFVRVORDAJAsw5FuTaJEhWM2SHB3mOAlhkNxwuLzeJsGwqWzf5TFNdKgtY5qHp6ZFf67Y/sAVadCaVY5YACDDb3Oi4NIjLnWMw2QthCBIsVhsUTU9tvXsjeq9+X1d75/KEs4LNOfcdf/+HthMnvwxOD0wmHaXr7ZItn2wuH2SnBzbZAbPJwpPx+VQuzcm7dgRCB57a1uBzUDRL4bfnI0RE0eaXd9W89mpjqHZnUI5Hh2l2dkZZUhOqpi2qSmpOmZ64Tuu9qlz/SEXo6MEHa3wOip46F1n7633eekV8ds8Wxjn37Wl63VVa+ej5oeEZ/82ZBETJjpJ1Rbij2D3Z/1trXUvLsblCK0XfOx0SX2kMsn9dX+d+7Kf6h8o4AIykuffjT8L20LU+w4AZd5VvEPY+XpWqLV327HR7DzXuDnD8r+ovkBehJ8i+y8YAAAAASUVORK5CYII='"+
				" />&nbsp; <span id='new-messages'>Nuovi messaggi</span>"+
				"<button class='close' id='close' >&times;</button></div>" +
				"<div id='msg' data-notify-html='msg'/></div>" +
		"</div>"	
	});

	$(document).on('click', '.notifyjs-newMsg-base #msg', function() {
		FIRST_TIME=true;
		if (nChat>1)
			window.location.href="gruppi.html";
		else
		{
			setTempCookie('groupName',nomeChat);
			setTempCookie('groupId',idChat);
			window.location.href="messaggi.html";
		}    
	});
	
	$(document).on('click', '.notifyjs-newMsg-base #close', function() {
		$('#newMsg').trigger('notify-hide'); 
	});
	
	$('#newMsg').trigger('notify-hide');
	
	var txt;
	if (nMsg==1)
		txt='1 '+$translation['new-message-from']+' '+nomeChat;
	else
	{
		if (nChat==1)
			txt=nMsg+' '+$translation['new-messages-from']+' '+nomeChat;
		else
			txt=nMsg+' '+$translation['new-messages-from']+' '+nChat+' '+$translation['chats'];
	}
	$.notify({
		msg: txt
	},{ 
		style: 'newMsg',
		clickToHide: false,
		autoHide: false,
		position: 'top left'
	});
	var string=$translation['new-messages'];
	document.getElementById('new-messages').innerHTML=string.charAt(0).toUpperCase() + string.slice(1);
	
	//chiusura al drag
	//pc	
	document.addEventListener("dragstart", function(event) {
		$('#newMsg').trigger('notify-hide');
	});
	//smartphone
	document.getElementById('newMsg').addEventListener('touchmove', function(event) {
		$('#newMsg').trigger('notify-hide');
		event.preventDefault();
	}, false);
	
	//settaggio stile notifica 
	var box=document.getElementById('newMsg');
	box.draggable=true;
	box.style.cursor='default';
	box.style.opacity='0.85';
	box.style.width='200px';
	box.style.padding='10px';
	box.style.color='#3A87AD';
	box.style.backgroundColor='#D9EDF7';
	box.style.border='1px solid #BCE8F1';
	box.style.borderRadius='8px';
	box.style.textAlign='center';
	
	var title=document.getElementById('title');
	title.style.fontWeight='bold';
	title.style.fontSize='18px';
	
	var msg=document.getElementById('msg');
	msg.style.padding='10px 0px 20px 0px';
	msg.style.cursor='pointer';
	
	document.getElementById('close').style.cursor='pointer';
	
	box.style.height=document.getElementById('clearfix').offsetHeight+'px';
}
