const PRIVATE='img/close.png';
const PUBBLIC='img/open.png';

function initAccount()
{
	$("#pic").attr("src","../server/?op=get&type=avatar&file="+getTempCookie('loginData1'));
	document.getElementById('loadPic').value='';
	document.getElementById('user').value=getTempCookie('loginData1');
	document.getElementById('nome').innerHTML=getTempCookie('loginData2');
	document.getElementById('email').innerHTML=getTempCookie('loginData3');
	setTimeout(getMyGroups,10);
}

function changeName()
{
	var name=document.getElementById('newName').value;
	if (name=='')
	{
		setNotify('newName',$translation['insert-name'],'error','top center');
		return;
	}
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			userId: getCookie("auth_id"),
			newName: name,
			op: "changeName"
		} ,
		success: function(response){
			var data=JSON.parse(response);
			if(data)
			{
				$('#nameModal').modal('hide');
				setNotify('nameChangedNotify',$translation['name-updated'],'success','top center');
				setTempCookie('loginData2',name);
				document.getElementById('nome').innerHTML=name;
			}		
			else 
				setNotify('newNameNotify',$translation['server-offline'],'error','top center');
		} ,
		error: function(response){
			setNotify('newNameNotify',$translation['server-offline'],'error','top center');
		}
	});
}

function changePwd()
{
	var pwd=document.getElementById('pwd').value;
	var pwd1=document.getElementById('pwd1').value;
	var pwd2=document.getElementById('pwd2').value;
	var ret=false;
	if (pwd=='')
	{
		setNotify('pwd',$translation['insert-password'],'error','top center');
		ret=true;
	}
	if (pwd1=='')
	{
		setNotify('pwd1',$translation['insert-new-password'],'error','top center');
		ret=true;
	}
	if (pwd2=='')
	{
		setNotify('pwd2',$translation['confirm-password'],'error','top center');
		ret=true;
	}
	if (ret) return;
	if (pwd1!=pwd2)
	{
		setNotify('pwd1',$translation['pwd-differs'],'error','top center');
		setNotify('pwd2',$translation['pwd-differs'],'error','top center');
		return;
	}
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			userId: getCookie("auth_id"),
			oldPwd: pwd,
			newPwd: pwd1,
			op: "changePwd"  
		} ,
		success: function(response){
			var data=JSON.parse(response);
			if (data=='error')
				setNotify('pwd',$translation['wrong-password'],'error','top center');
			else if (data)
			{
				$('#pwdModal').modal('hide');
				setNotify('pwdChangedNotify',$translation['password-updated'],'success','top center');
			}
			else 
				setNotify('newPwdNotify',$translation['server-offline'],'error','top center');
		} ,
		error: function(response){
			setNotify('newPwdNotify',$translation['server-offline'],'error','top center');
		}
	});
}

function clearInput()
{
	document.getElementById('newName').value='';
	document.getElementById('pwd').value='';
	document.getElementById('pwd1').value='';
	document.getElementById('pwd2').value='';
	$('#pwd1').keyup();
}

function getMyGroups()
{
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			idUser: getCookie("auth_id"),
			op: "getMyGroups"  
		} ,
		success: function(response){
			var data=JSON.parse(response);
			if (data=='error')
				setNotify('menu',$translation['server-offline'],'error','buttom center');
			else
			{
				if (data.length>0)
				{
					var container=document.getElementById('chatContainer');
					container.innerHTML='';
					var table=document.createElement('table');
					table.setAttribute('class','table table-hover');
					var thead=document.createElement('thead');
					var tr=document.createElement('tr');
					txt=[$translation['name'],$translation['description'],$translation['type']];
					for(var k=0;k<txt.length;k++)
					{
						var th=document.createElement('th');
						th.innerHTML=txt[k];
						tr.appendChild(th);
					}
					thead.appendChild(tr);
					table.appendChild(thead);
					var tbody=document.createElement('tbody');
					tbody.setAttribute('id','myChatBody');
					table.appendChild(tbody);
					container.appendChild(table);
				}
				for (var k=0;k<data.length;k++)
					addMyGroups('myChatBody',data[k]['id'],data[k]['nome'],data[k]['description'],data[k]['pwd']);
			}
			
		} ,
		error: function(response){
			setNotify('menu',$translation['server-offline'],'error','buttom center');
		}
	});
}

function addMyGroups(dadId,id,name,desc,pwd)
{
	var tbody=document.getElementById(dadId);
	var tr=document.createElement('tr');
	tr.setAttribute('id','chat_'+id);
	tr.setAttribute('style','cursor:pointer');
	tr.setAttribute('onclick','openChat('+id+')');
	var txt=[name,desc,pwd];
	for (var k=0;k<txt.length;k++)
	{
		var td=document.createElement('td');
		if (k==2)
		{
			var img=document.createElement('img');
			if (pwd)
				img.setAttribute('src',PRIVATE);
			else
				img.setAttribute('src',PUBBLIC);
			img.setAttribute('style','width:20px;height:20px');
			td.appendChild(img);
			td.setAttribute('class','text-center');
		}		
		else
			td.innerHTML=txt[k];
		tr.appendChild(td);
	}	
	tbody.appendChild(tr);
}

function openChat(id)
{
	console.log(id);
}

function delAccountSubmit()
{
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			idUser: getCookie("auth_id"),
			SU: 0,
			op: "deleteUser"  
		} ,
		success: function(response){
			var data=JSON.parse(response);
			if(data)
			{
				setRealCookie('loginOption','account_deleted');
				logout();	
			}
			else 
				setNotify('delAccountNotify',$translation['server-offline'],'error','top center');
		} ,
		error: function(response){
			setNotify('delAccountNotify',$translation['server-offline'],'error','top center');
		}
	});
}

//avatar
var pic;

function caricaPic(){
        var reader = new FileReader();
        reader.onload = function(){
                pic = reader.result;
		editPic();
        };
        reader.readAsDataURL($("#loadPic").prop("files")[0]);
}

function editPic(){
	if (pic===undefined)
	{
		setNotify('loadPic',$translation['update-picture'],'error','top center');
		return;
	}
	var data = new FormData();
	data.append('user', getTempCookie('loginData1'));
	data.append('pic', pic);
	data.append('op', 'changeAvatar');
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: data, 
		processData: false,
   		contentType: false,
		success: function(response){
			var data=JSON.parse(response);
			if (!data)
				setNotify('editAvatar',$translation['avatar-error'],'error','buttom center');
			else
				$("#pic").attr("src","../server/?op=get&type=avatar&file="+getTempCookie('loginData1')+"&update="+new Date().getTime());
		} ,
		error: function(response){
			setNotify('editAvatar',$translation['avatar-error'],'error','buttom center');
		}
	});
}
