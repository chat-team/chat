var $translation = null;

$(document).ready(function(){
	cerca_lingua();
});

function cerca_lingua(){
	var lang = (getRealCookie('lang') == "") ? navigator.language.substr(0,2) :  getRealCookie('lang');
	setRealCookie('lang',lang,100);
	if(getTempCookie(lang+'.data')==null)
		scarica_file();
	else
		carica_file();
}

function cambia_lingua(lang){
	if(lang == getRealCookie('lang')) return;
	setRealCookie('lang',lang,100);
	location.reload();
}

function scarica_file(){
	$.ajax({
		url:'../server/',
		type: 'GET',
		data: {op: 'language'},
		success: function(response){
			setTempCookie(getRealCookie('lang')+'.data',response.replace(/[\t\n]+/g,''));
			carica_file();
		}
	});
}

function carica_file(){
	var res = JSON.parse(getTempCookie(getRealCookie('lang')+'.data'));
	for(el in res.text)
		$('[translation='+el+']').html(res.text[el]);
	for(el in res.value)
		$('[translation='+el+']').val(res.value[el]);
	for(el in res.placeholder)
		$('[translation='+el+']').attr('placeholder',res.placeholder[el]);
	$translation = res.variable;
	try{$('[feature="pwd-strength"]').password(res.password);}catch(e){}
	try{$('[feature="emoji-picker"]').emojioneArea(res.emoji);}catch(e){}
}
