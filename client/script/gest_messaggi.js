var LAST_ID=0; //id ultimo msg ricevuto
var LAST_DATE=0; //data dell'ultimo msg ricevuto
var dateCounter=0;
var msgCounter=0;
var userColor=new Array(); //contiene il colore della scritta degli utenti
var BLOCK_REQUEST=false; //blocca la richiesta nuovi msg se quella precedente non ha finito

function initMessage()
{	
	if (getTempCookie('groupName')=='')
		window.location.href='gruppi.html';
	document.getElementById('chatName').innerHTML=getTempCookie('groupName');
	getMessages(1);
	setInterval(getMessages,1000);
	
	//verifica ultimo accesso utenti
	getLastAccess(getTempCookie('groupId'));
	setInterval(getLastAccess,10000,getTempCookie('groupId'));
	
	//setta giusta altezza elementi
	setDivHeight();
	
	//gestione touch userList
	gestTouch();
	
	//eToDo: gestire il cambio del giorno se utente è online (passata la mezzanotte)
}

function openChat()
{
	console.log(getTempCookie('groupId'));
}

function checkInvio(event)
{
	var ev=event.keyCode;
	if (ev==13)
	{
		;
	}
}

function getMessages(newMsg=0) //newMsg -> se true indica la presenza di nuovi messaggi (graficamente)
{
	if (BLOCK_REQUEST) return;
	BLOCK_REQUEST=true;
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			op: "receive",
			userId: getCookie("auth_id"),
			lastId: LAST_ID,
			groupId: getTempCookie('groupId')
		} ,
		success: function(response){
			BLOCK_REQUEST=false;
			var data=JSON.parse(response);
			var first=true;
			var lastDate;
			var lastAllRead=data.pop()['lastAllRead']; //id ultimo msg letto da tutti (spunta blu)
			for (var k=0;k<data.length;k++)
			{
				if (newMsg==1 && data[k]['newMsg'] && first) //msg non letti
				{
					first=false;
					msgNotRead(data.length-k);
				}
				//check last date
				if (k==0)
				{
					if (LAST_DATE==0)
						lastDate=data[k]['data'];
					else
						lastDate=LAST_DATE;
				}
				else
					lastDate=data[k-1]['data'];
					
				if (data[k]['user']==getTempCookie('loginData1'))
					insertMyMsg(data[k]['txt'],data[k]['data'],(LAST_ID || k),lastDate,first,newMsg,data[k]['id']);
				else
					insertOtherMsg(data[k]['user'],data[k]['txt'],data[k]['data'],data[k-((k==0)?0:1)]['user'],(LAST_ID || k),lastDate,first,newMsg);
				if (k+1==data.length)
				{
					LAST_DATE=data[k]['data'];
					LAST_ID=data[k]['id'];
				}
			}	
			if (!first) //se ho almeno un nuovo msg scrollo
			{
				var msgbox=document.getElementById('msgbox');
				var div=document.getElementById('msgNotRead');
				if (msgbox.offsetHeight-150<msgbox.scrollTop)
					msgbox.scrollTop+=msgbox.offsetHeight-150;
			}
			//fix ultimo msg letto (spunta blu)
			for (var k=1;k<=lastAllRead;k++)
			{
				try{
					document.getElementById('msgId_'+k).setAttribute('src','img/msgRead.png');
				}catch(e){}
			}
			//apertura img al clik
			var img=getAllImg();
			for (var k=0;k<img.length;k++)
			{
				img[k].setAttribute('id','img_'+k);
				img[k].setAttribute('onclick','openImg("img_'+k+'")');
				img[k].setAttribute('style','cursor:pointer');
			}
			setMsgPress();
		} ,
		error: function(response){
			setNotify("menu",$translation['client-offline'],'warn','buttom left',true,true,1000,0,0);
		}
	});
}

var TOUCH_CTR=false; //controllo sul touch (vedi funzione sotto)

function setMsgPress()
{
	var msg=document.getElementById('msgbox').getElementsByTagName('table');
	for (var k=0;k<msg.length;k++)
	{
		msg[k].setAttribute('onmousedown','mouseDown(this)');
		msg[k].setAttribute('onmouseup','mouseUp()');
		//smartphone
		
		msg[k].addEventListener('touchstart', function(event) {
		//	event.preventDefault();
			if(!TOUCH_CTR){
				TOUCH_CTR=true;
				var el=event.target;
				while(el.tagName!='TABLE') el=el.parentNode;
				mouseDown(el);
				mouseDown(el);
			}
		},{passive: true});
		
		msg[k].addEventListener('touchend', function(event) {
	//		event.preventDefault();
			TOUCH_CTR=false;
			mouseUp();
		},{passive: true});
	}
}

var SEL_MSG={length:0};
var PRESS_CHECK;

function mouseDown(el)
{
	clearTimeout(PRESS_CHECK);
	if (SEL_MSG.length>0) //se ho già selezionato qualcosa
	{
		if (SEL_MSG[el.id])	deSelMsg(el);
		else selMsg(el);
	}
	else
		PRESS_CHECK=setTimeout(selMsg,500,el);
}

function mouseUp()
{
	clearTimeout(PRESS_CHECK);
}

function selMsg(el)
{
	SEL_MSG[el.id]=true;
//	if (SEL_MSG.length==0)	SEL_MSG.first=el.id;
	SEL_MSG.length++;
	el.setAttribute('style','background-color:blue');
}

function deSelMsg(el)
{
	SEL_MSG[el.id]=false;
	SEL_MSG.length--;
	el.setAttribute('style','background-color:none');
}

function getAllImg()
{
	var msg=document.getElementsByClassName('msg');
	var img=new Array(),tmp;
	for (var k=0;k<msg.length;k++)
	{
		tmp=msg[k].getElementsByTagName('img')[0];
		if (tmp!=undefined && tmp.id.split('_')[0]!='msgId') img.push(tmp);
	}
	return img;
}

function openImg(id)
{
	if (SEL_MSG.length>0)	return;
	console.log(id);
}

function sendMessage(check)
{
	var txt=document.getElementById("msgText").value;
	if (txt=="")
		return;
	document.getElementById("msgText").value="";
	if (check)
		txt=urlToLink(tagToString(txt));
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			user: getCookie("auth_id"),
			txt: txt,
			groupId: getTempCookie('groupId'), 
			op: "send"  
		} ,
		success: function(response){
			if(!(JSON.parse(response)))
				setNotify('insMsg',$translation['send-msg-error'],'error','top center');
			else $("#input").text("");
		} ,
		error: function(response){
			setNotify('insMsg',$translation['send-msg-error'],'error','top center');
		}
	});
}

function setMsgWidth()  //settaggio larghezza dinamica
{
	var msgbox=document.getElementById('msgbox');
	var width="width:"+parseInt(msgbox.clientWidth*0.75)+'px;';
	var space="width:"+parseInt(msgbox.clientWidth*0.25)+'px;';	
	for (var k=0;k<msgCounter;k++)
	{
		var el=document.getElementById('msg'+k);
		el.setAttribute('style',width);
		el=document.getElementById('space'+k);
		el.setAttribute('style',space);
	}
	setPositionDataMsg();
}

function setPositionDataMsg() //centra la data
{
	var databox=document.getElementById('dataMsg');
	var left=parseInt(document.body.clientWidth/2-databox.clientWidth/2-2)+'px';
	databox.style.left=left;
	var msgbox=document.getElementById('msgbox');
	var top=getPosition(msgbox).y+2;
	databox.style.top=top+'px';
}

function setDivHeight()  //seleziona la giusta altezza degli elementi
{
	var menu=document.getElementById('menu').offsetHeight;
	var msgbox=document.getElementById('msgbox');
	var insMsg=document.getElementById('insMsg').offsetHeight;
	msgbox.style.height=(window.innerHeight-menu-insMsg)+'px';
}

function setDataMsg() //visualizza data
{
	document.getElementById('dataMsg').style.display='block';
	document.getElementById('dataMsg').innerHTML=document.getElementById('date'+(dateCounter-((dateCounter==0)?0:1))).innerHTML;
	for (k=0;k<dateCounter;k++)
	{
		if ($("#date"+k).position().top>100) //100 è l'altezza del span date (circa)
		{
			document.getElementById('dataMsg').innerHTML=document.getElementById('date'+(k-((k==0)?0:1))).innerHTML;
			break;
		}
	}
	setPositionDataMsg();
	setTimeout(function(){document.getElementById('dataMsg').style.display='none';},2000);
}

function changeDay(msgbox,date) //cambia il giorno
{
	var el=document.createElement('span');
	el.setAttribute('class','changeDay');
	el.setAttribute('id','date'+dateCounter);
	dateCounter++;	
	el.innerHTML=getDateTxt(date,false);
	msgbox.appendChild(document.createElement('br'));
	msgbox.appendChild(el);
	msgbox.appendChild(document.createElement('br'));
	msgbox.appendChild(document.createElement('br'));
}

function msgNotRead(num) //numero di msg non letti
{
	var msgbox=document.getElementById('msgbox');
	var txt='1 '+$translation['new-message'];
	if (num>1)	txt=+num+' '+$translation['new-messages'];
	var div=document.createElement('div');
	div.setAttribute('id','msgNotRead');
	var span=document.createElement('div');
	span.setAttribute('class','msgNotRead');
	span.innerHTML=txt;
	var br=document.createElement('br');
	div.appendChild(br);
	div.appendChild(span);
	var br=document.createElement('br');
	div.appendChild(br);
	msgbox.appendChild(div);

	msgbox.scrollTop+=(div.offsetHeight+1000)*1000;
}

function openUser(user)
{
	if (SEL_MSG.length>0)	return;
	console.log(user);
}

function insertOtherMsg(user,txt,date,oldUser,first,oldDate,scroll,newMsg)
{
	var msgbox=document.getElementById('msgbox');

	if (first==0 || Math.abs(date.split(' ')[0].split('-')[2]-oldDate.split(' ')[0].split('-')[2])>0)  //cambiato il giorno
	{
		changeDay(msgbox,date);
		first = 0;
	}
		
	var width="width:"+parseInt(msgbox.clientWidth*0.75)+'px;';	
	var space="width:"+parseInt(msgbox.clientWidth*0.25)+'px;';

	if (newMsg==0) //se arrivano altri msg quando sono online la scritta nuovi msg sparisce
	{
		try{document.getElementById('msgNotRead').style.display='none';}catch(e){}
	}

	var table=document.createElement('table');
	table.setAttribute('id','table'+msgCounter);
	if (user!=oldUser || first==0)
	{
		var tr=document.createElement('tr');
		var td=document.createElement('td');
		td.setAttribute('style',width);
		td.setAttribute('colspan','2');
		td.innerHTML="<img class='userpic' src='../server/?op=get&type=avatar&file="+user+"' />";
		if (user==null)
		{
			td.innerHTML+=$translation['unknown'];
			td.setAttribute('style','text-align:left');
		}
		else
		{
			td.innerHTML+=user;
			td.setAttribute('style','cursor:pointer;font-weight:bold;text-align:left');
			td.setAttribute('onclick','openUser("'+user+'")');
			td.style.color=getUserColor(user);
		}
		tr.appendChild(td);
		table.appendChild(tr);
	}
	var tr=document.createElement('tr');
	var td=document.createElement('td');
	var div=document.createElement('div');
	div.setAttribute('class','otherMsgArrow');
	td.appendChild(div);
	td.setAttribute('style','vertical-align:top');
	tr.appendChild(td);
	
	var td=document.createElement('td');
	td.setAttribute('class','msg other');
	td.setAttribute('style',width);
	td.setAttribute('id','msg'+msgCounter);
	td.innerHTML=txt;
	var el=document.createElement('p');
	el.setAttribute('class','sendDate');
	el.innerHTML=getDateTxt(date,true);
	td.appendChild(el);
	tr.appendChild(td);
	
	var td=document.createElement('td');
	td.setAttribute('style',space);
	td.setAttribute('id','space'+msgCounter);
	tr.appendChild(td);
	msgCounter++;
	table.appendChild(tr);
	
	msgbox.appendChild(table);
	
	if(scroll) //i nuovi msg non vanno scrollati
		msgbox.scrollTop+=(table.offsetHeight+1000)*1000;
}

function insertMyMsg(txt,date,first,oldDate,scroll,newMsg,msgId)
{
	var msgbox=document.getElementById('msgbox');
	
	if (first==0 || Math.abs(date.split(' ')[0].split('-')[2]-oldDate.split(' ')[0].split('-')[2])>0)  //cambiato il giorno
		changeDay(msgbox,date);
	
	if (newMsg==0) //se arrivano altri msg quando sono online la scritta nuovi msg sparisce
	{
		try{
			document.getElementById('msgNotRead').style.display='none';
		}catch(e){}
	}
	
	var space="width:"+parseInt(msgbox.clientWidth*0.25)+'px;';
	var width="width:"+parseInt(msgbox.clientWidth*0.75)+'px;';
		
	var table=document.createElement('table');
	table.setAttribute('id','table'+msgCounter);
	var tr=document.createElement('tr');
	var td=document.createElement('td');
	td.setAttribute('style',space);
	td.setAttribute('id','space'+msgCounter);
	tr.appendChild(td);
	var td=document.createElement('td');
	td.setAttribute('class','msg my');
	td.setAttribute('style',width);
	td.setAttribute('id','msg'+msgCounter);
	msgCounter++;
	td.innerHTML=txt;
	var el=document.createElement('p');
	el.setAttribute('class','sendDate myData');
	el.innerHTML=getDateTxt(date,true)+'&nbsp;';
	var read=document.createElement('img');
	read.setAttribute('src','img/msgSend.png');
	read.setAttribute('id','msgId_'+msgId);
	read.setAttribute('style','height:15px;width:15px');
	el.appendChild(read);
	td.appendChild(el);
	tr.appendChild(td);
	
	var td=document.createElement('td');
	var div=document.createElement('div');
	div.setAttribute('class','myMsgArrow');
	td.appendChild(div);
	td.setAttribute('style','vertical-align:top');
	tr.appendChild(td);
	
	table.appendChild(tr);
	
	msgbox.appendChild(table);
	
	if(scroll) //i nuovi msg non vanno scrollati
		msgbox.scrollTop+=(table.offsetHeight+1000)*1000;
}

function tagToString(text)
{
	var newtext="";
	for(var i=0; i<text.length; i++){
		if(text[i]=="<") newtext+="&lt;";
		else if(text[i]==">") newtext+="&gt;";
		else newtext+=text[i];
	}
	return newtext;
}

function urlToLink(text){
	text = text.split(" ");
	for(var i = 0; i < text.length; i++)
		if(isURL(text[i]))
			text[i] = "<a href='"+text[i]+"'>"+text[i]+"</a>";
	return text.join(" ");
}

function isURL(str) {
	var pattern = new RegExp('^(https?)://[^\s/$.?#].[^\s]*$','i');
	return pattern.test(str);
}

// allegati

var file;
var name;

function pictureUpload(){
	$("#msgText").val("<img src='../server/?op=get&type=attachment&id="+getTempCookie('groupId')+"&file="+md5(file)+"&name="+name+"' title='"+name+"'/>");
	sendFile();
}

function attachmentUpload(){
	$("#msgText").val("<a href='../server/?op=get&type=attachment&id="+getTempCookie('groupId')+"&file="+md5(file)+"&name="+name+"'>"+name+"</a>");
	sendFile();
}

function sendFile(){
	var data = new FormData();
	data.append('file', file);
	data.append('id',getTempCookie('groupId'));
	data.append('op', 'uploadFile');
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: data,
		processData: false,
   		contentType: false,
		success: function(response){
			if(!(JSON.parse(response)))
				setNotify('insMsg',$translation['send-file-error'],'error','top center');
			else
				abortUpload();
		} ,
		error: function(response){
			setNotify('insMsg',$translation['send-file-error'],'error','top center');
		}
	});
	sendMessage(false);
}

function load(id){
	if($('#'+id).val()=="")return;
	var reader = new FileReader();
	reader.onload = function(){
			file = reader.result;
	};
	reader.readAsDataURL($('#'+id).prop("files")[0]);
	name = $('#'+id).val().replace(/.*[\/\\]/, '');
	$('#send').attr('name',"Carica");
	$('#send').attr('onclick',id+'Upload()');
	$("#msgFile").val("file:///"+name);
	$("#msgText").val("file:///"+name);
	$("#iText").attr('hidden','true');
	$("#iFile").removeAttr('hidden');
	
}

function abortUpload(){
	if($('#send').attr('name')=='Carica'){
		$('#send').attr('name',"Invia");
		$('#send').attr('onclick','sendMessage(true)');
		$("#attachment").val("");
		$("#picture").val("");
		$("#iFile").attr('hidden','true');
		$("#iText").removeAttr('hidden');
	}
}

$(document).ready(function(){
	setTimeout(function(){
		$(".emojionearea-editor").attr('id','input');
	}, 50);
	setTimeout(function(){
		$(".emojionearea-editor").attr('id','input');
	}, 1000);
});

//creazione colori nome utenti
function getUserColor(user)
{
	if (userColor[user]==undefined) 
		userColor[user]=createRandomColor();
	return userColor[user];
}

function createRandomColor()
{
	var arrayColor=[0xf71313,0xe05e02,0xedb009,0xbab700,0x85e516,0x60e55e,0x74cc91,0x215633,0x00edff,0x022d84,0x6a4bd1,0x2a1866,0x29083f,0xdc04ef,0xff68de,0xff687e,0x962e2e,0x77ea86,0xe5b830,0xe5b830];
	var index=Math.round((arrayColor.length-1)*Math.random());
	return ('#'+arrayColor[index].toString(16));
}

//apertura userList al touch
var touchX;
function gestTouch()
{
	document.getElementById('userButton').addEventListener('touchstart', function(event) {
		touchX=event.changedTouches[0].clientX;
	}, {passive: true});
	document.getElementById('userButton').addEventListener('touchend', function(event) {
		if (event.changedTouches[0].clientX>touchX)	$('#userModal').modal('toggle');
	}, {passive: true});
}
