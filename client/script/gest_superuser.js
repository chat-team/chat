function initSuperuser()
{
	setInterval(getAllData,30000);
}

function checkSU()
{
	var user=document.getElementById('SU_user').value;
	var pwd=document.getElementById('SU_pwd').value;
	var ret=false;
	if (user=='')
	{
		setNotify('SU_user',$translation['insert-username'],'error','top center');
		ret=true;
	}
	if (pwd=='')
	{
		setNotify('SU_pwd',$translation['insert-password'],'error','top center');
		ret=true;
	}
	if (ret) return;
	
	if (user!=getTempCookie('loginData1'))
	{
		setNotify('checkSUButton',$translation['incorrect-login'],'error','top center');
		return;
	}
	
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: {
			user: user, 
			id: getCookie("auth_id"),
			pwd: pwd,
			op: "checkSU"  
		} ,
		success: function(response){
			var data=JSON.parse(response);
			if (data)
			{	
				document.getElementById('checkSU').style.display='none';
				document.getElementById('checkSUConfirmed').style.display='block';
				getAllData();
			}
			else
				setNotify('checkSUButton',$translation['incorrect-login'],'error','top center');
			
		} ,
		error: function(response){
			setNotify('checkSUButton',$translation['server-offline'],'error','top center');
		}
	});

}

function checkInvio(event)
{
	if (event.keyCode==13)
	{
		if (document.getElementById('checkSU').style.display!='none')
			checkSU();
	}
}

function getAllData()
{
	getAllUsers();
	getAllChat();
}

function getAllUsers() //get users
{	
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: {
			op: "getAllUsers"  
		} ,
		success: function(response){
			var data=JSON.parse(response);
			var b1=true,b2=true;
			for (var k=0;k<data.length;k++)
			{
				if (b1 && data[k]['block'])
				{
					b1=false;
					document.getElementById('userBannedContainer').innerHTML='';
				}
				else if (b2 && !data[k]['block'])
				{
					b2=false;
					document.getElementById('userContainer').innerHTML='';
				}
				insertUser(data[k]['user'],data[k]['id'],data[k]['name'],data[k]['email'],data[k]['SU'],data[k]['block'],data[k]['ultimo_accesso'],data[k]['fine_blocco']);
			}
		} ,
		error: function(response){
			setNotify('menu',$translation['server-offline'],'error','bottom center');
			setNotify('menu_smartphone',$translation['server-offline'],'error','bottom center');
		}
	});
}

function getAllChat() //get chats
{	
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: {
			op: "getAllChat"  
		} ,
		success: function(response){
			var data=JSON.parse(response);
			console.log(data);
			
		} ,
		error: function(response){
			setNotify('menu',$translation['server-offline'],'error','bottom center');
			setNotify('menu_smartphone',$translation['server-offline'],'error','bottom center');
		}
	});
}

function changeOpt(opt)
{
	if (opt)
	{
		document.getElementById('gestUtenti').style.display='none';
		document.getElementById('gestChat').style.display='block';
		document.getElementById('opt1').setAttribute('class','optTable optDesel');
		document.getElementById('opt2').setAttribute('class','optTable optSel');
	}
	else
	{
		document.getElementById('gestUtenti').style.display='block';
		document.getElementById('gestChat').style.display='none';
		document.getElementById('opt2').setAttribute('class','optTable optDesel');
		document.getElementById('opt1').setAttribute('class','optTable optSel');
	}
}

function insertUser(user,id,name,email,SU,block,lastAccess,fineBlocco)
{
	var dadId='userContainer';
	if (block) dadId='userBannedContainer';
	
	var box=document.getElementById(dadId);
		
	var table=document.createElement('table');
	table.setAttribute('class','selGroup');
	table.setAttribute('id','user_'+id);
	if (block)
		table.setAttribute('onclick','bannedUserClick("'+user+'","'+id+'","'+name+'","'+email+'","'+lastAccess+'","'+fineBlocco+'")');
	else
		table.setAttribute('onclick','userClick("'+user+'","'+id+'","'+name+'","'+email+'","'+SU+'","'+lastAccess+'")');

	var tr=document.createElement('tr');
	
	var arr=[user,name];
	for (var k=0;k<2;k++)
	{
		var td=document.createElement('td');
		var span=document.createElement('span');
		span.innerHTML=arr[k];
		td.setAttribute('style','width:50%');
		td.appendChild(span);
		tr.appendChild(td);
	}	

	table.appendChild(tr);
	box.appendChild(table);
}

function userClick(user,id,name,email,SU,lastAccess)
{
	userInfo([user,name,email,SU,lastAccess,id],true);
	document.getElementById('blockAccount').style.display='none';
}

function bannedUserClick(user,id,name,email,lastAccess,fineBlocco)
{
	userInfo([user,name,email,0,lastAccess,fineBlocco,id],false);
	document.getElementById('blockAccount').style.display='table-row';
}

function userInfo(arr,opt) //parte in comune delle due funzioni sopra
{
	document.getElementById('infoUser').style.display='block';
	document.getElementById('confermaEdit').style.display='none';
	document.getElementById('termineBan').style.display='none';
	document.getElementById('editAccount').style.display='none';
	document.getElementById('editBanAccount').style.display='none';
	document.getElementById('errorPermission').style.display='none';
	var id,txt;
	for (var k=0;k<arr.length-1;k++)
	{
		id='info_'+k;
		txt=arr[k];
		if (k==3)
		{
			if (arr[k]==1)
			{	
				txt='superuser';
				document.getElementById('errorPermission').style.display='block';
			}
			else
			{
				txt='user';
				
				if (opt) document.getElementById('editAccount').style.display='initial';
				else document.getElementById('editBanAccount').style.display='initial';
				
				document.getElementById('userId').setAttribute('name','userId_'+arr[arr.length-1]);
			}
		}
		if (k>3) txt=getInfoDataTxt(arr[k]);
		document.getElementById(id).innerHTML=txt;
	}
}

function getInfoDataTxt(data)
{
	var tmp=data.split(' ');
	var day=tmp[0].split('-');
	var hours=tmp[1].split(':');
	return day[2]+'/'+day[1]+'/'+day[0]+' '+hours[0]+':'+hours[1];
}

function delAccount()
{
	var id=document.getElementById('userId').name.split('_')[1];
	document.getElementById('submitButton').setAttribute('onclick','submitClick('+id+',0)');
	document.getElementById('confermaEdit').style.display='block';
	document.getElementById('termineBan').style.display='none';
	document.getElementById('confMsg').innerHTML=$translation['confirm-delete'];
}

function bannAccount()
{
	var id=document.getElementById('userId').name.split('_')[1];
	document.getElementById('submitButton').setAttribute('onclick','submitClick('+id+',1)');
	document.getElementById('confermaEdit').style.display='block';
	document.getElementById('termineBan').style.display='block';
	document.getElementById('time').value='00:00';
	document.getElementById('confMsg').innerHTML=$translation['confirm-ban'];
}

function promUser()
{
	var id=document.getElementById('userId').name.split('_')[1];
	document.getElementById('submitButton').setAttribute('onclick','submitClick('+id+',2)');
	document.getElementById('confermaEdit').style.display='block';
	document.getElementById('termineBan').style.display='none';
	document.getElementById('confMsg').innerHTML=$translation['confirm-promotion'];
}

function sbanAccount()
{
	var id=document.getElementById('userId').name.split('_')[1];
	document.getElementById('submitButton').setAttribute('onclick','submitClick('+id+',3)');
	document.getElementById('confermaEdit').style.display='block';
	document.getElementById('termineBan').style.display='none';
	document.getElementById('confMsg').innerHTML=$translation['confirm-ban-end'];
}

function submitClick(userId,op)
{
	switch(op)
	{
		case 0: //delete account
			$.ajax({
				url:'../server/index.php',
				type: 'POST',
				data: {
					op: "deleteUser",
					idUser: userId,
					SU:1 
				} ,
				success: function(response){
					var data=JSON.parse(response);
					if (data=='error' || !data)
						setNotify('confermaEdit',$translation['server-offline'],'error','bottom center');
					else
					{
						getAllUsers();
						document.getElementById('infoUser').style.display='none';
						setNotify('normalUsers',$translation['account-deleted'],'success','bottom center');
					}	
				} ,
				error: function(response){
					setNotify('menu',$translation['server-offline'],'error','bottom center');
					setNotify('menu_smartphone',$translation['server-offline'],'error','bottom center');
				}
			});
		break;
		case 1: //ban user
			var data=document.getElementById('date').value;
			var time=document.getElementById('time').value;
			var ret=false;
			if (data=='')
			{
				setNotify('date',$translation['insert-date'],'error','top center');
				ret=true;
			}
			if (time=='')
			{
				setNotify('time',$translation['insert-time'],'error','top center');
				ret=true;
			}
			if (ret) return;
			data+=' '+time+':00';
			var currentDate=new Date();
			var data1=new Date(data);
			var diff=data1.getTime() - currentDate.getTime();
			diff=Math.floor(diff / (1000 * 3600 * 24));
			if (diff<1)
			{
				setNotify('termineBanNotify',$translation['minimum-ban'],'error','top center');
				return;
			}
			$.ajax({
				url:'../server/index.php',
				type: 'POST',
				data: {
					op: "banUser",
					userId: userId,
					banDate: data  
				} ,
				success: function(response){
					var data=JSON.parse(response);
					if (data=='error' || !data)
						setNotify('confermaEdit',$translation['server-offline'],'error','bottom center');
					else
					{
						getAllUsers();
						document.getElementById('infoUser').style.display='none';
						setNotify('bannedUsers',$translation['ban-success'],'success','top center');
					}	
				} ,
				error: function(response){
					setNotify('menu',$translation['server-offline'],'error','bottom center');
					setNotify('menu_smartphone',$translation['server-offline'],'error','bottom center');
				}
			});
		break;
		case 2: //promuove utente
			$.ajax({
				url:'../server/index.php',
				type: 'POST',
				data: {
					op: "upToSU",
					userId: userId 
				} ,
				success: function(response){
					var data=JSON.parse(response);
					if (data=='error' || !data)
						setNotify('confermaEdit',$translation['server-offline'],'error','bottom center');
					else
					{
						getAllUsers();
						document.getElementById('infoUser').style.display='none';
						setNotify('normalUsers',$translation['promotion-success'],'success','bottom center');
					}
				} ,
				error: function(response){
					setNotify('menu',$translation['server-offline'],'error','bottom center');
					setNotify('menu_smartphone',$translation['server-offline'],'error','bottom center');
				}
			});
		break;
		case 3: //termine ban utente
			$.ajax({
				url:'../server/index.php',
				type: 'POST',
				data: {
					op: "deleteBan",
					userId: userId 
				} ,
				success: function(response){
					var data=JSON.parse(response);
					if (data=='error' || !data)
						setNotify('confermaEdit',$translation['server-offline'],'error','bottom center');
					else
					{
						getAllUsers();
						document.getElementById('infoUser').style.display='none';
						setNotify('normalUsers',$translation['reenable-success'],'success','bottom center');
					}
				} ,
				error: function(response){
					setNotify('menu',$translation['server-offline'],'error','bottom center');
					setNotify('menu_smartphone',$translation['server-offline'],'error','bottom center');
				}
			});
		break;
	}
}
