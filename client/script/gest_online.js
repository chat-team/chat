function setLastAccess()
{
	setLastAccess2();
	setInterval(setLastAccess2,10000);
}

function setLastAccess2()
{
	var page=window.location.pathname;
	page=page.split('/');
	page=page[page.length-1];
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			op: "setAccess",
			groupId: (page=='messaggi.html')?getTempCookie('groupId'):0,
			getLast: (page=='gruppi.html')?1:0,
			userId: getCookie("auth_id")
		} ,
		success: function(response){
			var data=JSON.parse(response); //NB L'ultima cella del vettore viene usata per il SU check
			if (data=='error')
				logout();
			else if (!data)
				setNotify("menu",$translation['server-offline'],'error','buttom center',true,true,10000,0,0);
			else
			{
				//check SU
				var SU=data.pop()['SU']
				setTempCookie('loginData4',SU);
				//TODO: UPDATE THIS
				if (SU==0 && page=='superuser.html');
				//////////
				if (page=='gruppi.html') //aggiornare la lista gruppi
				{
					var totNewMsg=0;
					var update=false;
					for (var k=0;k<chatList.length;k++)
					{
						for (var i=0;i<data.length;i++)
						{
							if (chatList[k]['id']==data[i]['id'])
							{
								chatList[k]['last_all_read']=data[i]['last_all_read']; //check spunte blu
								if (data[i]['notify']>0)
								{	
									update=true;
									totNewMsg+=parseInt(data[i]['notify']);
									chatList[k]['new_msg']=data[i]['notify'];
									chatList[k]['last_msg']=data[i]['last_msg'];
									chatList[k]['last_msg_time']=data[i]['last_msg_time'];
									chatList[k]['last_sender']=data[i]['last_sender'];
								}
								break;
							}		
						}
					}
					if (update)
					{
						//riordino chatList
						chatSort();
						//update grafico
						document.getElementById('tableGroup').innerHTML='';
						for (var k=0;k<chatList.length;k++)
							insertGroup(chatList[k]);
						checkLastMsg(); 
					}
					checkBlue(); //check spunte blu
					
					//aggiorno nuovi msg totali
					$('#totNewMsg').html((totNewMsg==0)?'':totNewMsg);
				}
				else  //settare notifica
				{
					if (FIRST_TIME)
					{
						FIRST_TIME=false;
						for (var k=0;k<data.length;k++)
							gestNotify[k]=0;
					}
					var newNotify=false;
					var nMsg=0;
					var nChat=0;
					var index=0,only=true; //check unica chat
					for (var k=0;k<data.length;k++)
					{
						if (data[k]['notify']>gestNotify[k])
						{
							newNotify=true;
							gestNotify[k]=data[k]['notify'];
						}
						if (data[k]['notify']>0)
						{
							nMsg+=parseInt(data[k]['notify']);
							nChat++;
							if (nChat>1)	
								only=false;
							else
								index=k;
						}
					}
					if (newNotify)
					{
						if (only) //una sola chat
							setMsgNotify(nMsg,1,data[index]['name'],data[index]['id']);
						else
							setMsgNotify(nMsg,nChat);
					}		
				}
			}
		} ,
		error: function(response){
			setNotify("menu",$translation['client-offline'],'warn','buttom center',true,true,10000,0,0);
		}
	});
}

function chatSort()
{
	chatList.sort(sortFunction);
	function sortFunction(a, b) 
	{
		if (getUTC(a['last_msg_time']) === getUTC(b['last_msg_time']))	return 0; 
		return (getUTC(a['last_msg_time']) > getUTC(b['last_msg_time'])) ? -1 : 1;
	}
}

function getUTC(date)
{
	var tmp=date.split(' ');
	var yy=tmp[0].split('-')[0];
	var mm=tmp[0].split('-')[1];
	var dd=tmp[0].split('-')[2];
	var hh=tmp[1].split(':')[0];
	var mi=tmp[1].split(':')[1];
	var ss=tmp[1].split(':')[2];
	return Date.UTC(yy,mm,dd,hh,mi,ss);
}

function getLastAccess(id)  //ritorna gli ultimi accessi dell'utente dato un groupId
{
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			op: "getAccess",
			groupId: id
		} ,
		success: function(response){
			var data=JSON.parse(response);
			var container=document.getElementById('userbox');
			container.innerHTML='';
			var table=document.createElement('table');
			table.setAttribute('class','table');
			var thead=document.createElement('thead');
			var tr=document.createElement('tr');
			var txt=[$translation['username'],$translation['last-online']];
			for (var k=0;k<txt.length;k++)
			{
				var th=document.createElement('th');
				th.innerHTML=txt[k];
				tr.appendChild(th);
			}
			thead.appendChild(tr);
			table.appendChild(thead);
			var tbody=document.createElement('tbody');
			tbody.setAttribute('id','accessTableBody');
			table.appendChild(tbody);
			container.appendChild(table);
			
			for (var k=0;k<data.length;k++)
				addUserAccess(data[k]['user'],data[k]['last_access']);
		} ,
		error: function(response){
			setNotify("menu",$translation['client-offline'],'warn','buttom left',true,true,10000,0,0);
		}
	});	
}

function addUserAccess(name,access) 
{
	var tbody=document.getElementById('accessTableBody');
	var tr=document.createElement('tr');
	var td=document.createElement('td');
	td.innerHTML=name;
	td.setAttribute('class','text-dark');
	td.setAttribute('style','cursor:pointer');
	td.setAttribute('onclick','openUser("'+name+'")');
	tr.appendChild(td);
	var td=document.createElement('td');
	var dd=getDateTxt(access,false,true);
	td.innerHTML=((dd==$translation['today'] || dd==$translation['online'])?'':dd+' ')+((dd==$translation['online'])?dd:getDateTxt(access,true));
	if (td.innerHTML==$translation['online'])
		td.setAttribute('class','text-success');
	tr.appendChild(td);
	tbody.appendChild(tr);
}

function getPosition(obj) //ritorna la posizione di un oggetto
{
    var pos=Array();
    pos['left']=0;
    pos['top']=0;
    if(obj) 
    {
        while(obj.offsetParent) 
        {
            pos['left']+=obj.offsetLeft;//-obj.scrollLeft;
            pos['top']+=obj.offsetTop;//-obj.scrollTop;
            var tmp=obj.parentNode;
            while(tmp!=obj.offsetParent) 
            {
                pos['left']-=tmp.scrollLeft;
                pos['top']-=tmp.scrollTop;
                tmp=tmp.parentNode;
            }
            obj=obj.offsetParent;
        }
        pos['left']+=obj.offsetLeft;
        pos['top']+=obj.offsetTop;
    }
	return {x:pos['left'],y:pos['top']};
}

function getDateTxt(date,op,op2=false)  //calcola il testo da scrivere nel campo data op=true torna le ore altrimenti i giorni op2=true controllo online
{
	var days=date.split(' ')[0].split('-');  //aa-mm-gg
	var hour=date.split(' ')[1].split(':');  //hh-mm-ss
	if (op)
		return hour[0]+':'+hour[1];
	var currentDate=new Date(); 
	var currentDays=new Array(); //aa-mm-gg
	currentDays[0]=currentDate.getFullYear(); 
	currentDays[1]=currentDate.getMonth()+1;
	currentDays[2]=currentDate.getDate();

	if (op2)
	{
		var date1 = new Date(days[1]+'/'+days[2]+'/'+days[0]+' '+hour[0]+':'+hour[1]+':'+hour[2]);
		var date2 = new Date(currentDays[1]+'/'+currentDays[2]+'/'+currentDays[0]+' '+currentDate.getHours()+':'+currentDate.getMinutes()+':'+currentDate.getSeconds());
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffSecs = Math.floor(timeDiff / 1000);
		if (diffSecs < 20)
			return $translation['online'];
	}

	var date1 = new Date(days[1]+'/'+days[2]+'/'+days[0]);
	var date2 = new Date(currentDays[1]+'/'+currentDays[2]+'/'+currentDays[0]);
	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	var diffDays = Math.floor(timeDiff / (1000 * 3600 * 24)); 
	if (diffDays==0)
		return $translation['today'];
	if (diffDays==1)
		return $translation['yesterday'];
		
	return days[2]+'/'+days[1]+'/'+days[0];
}
