function checkCookie()
{
	var name="loginData";
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: {
			id: getCookie("auth_id"),
			key: getCookie("auth_key"),
			op: "autologin"
		},
		success: function(response){
			var data=JSON.parse(response);
//			console.log(data);
			if(data){
				setCookie("auth_key",data['key']);
				for (var k=0;k<6;k++)
					setTempCookie("loginData"+k,data['ret'][k]);
				window.location.href="gruppi.html";
			}
		}
	});
}

function checkCookie2(SU=false)
{
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data:{
			id: getCookie("auth_id"),
			key: getCookie("auth_key"),
			op: "autologin"
		},
		success: function(response){
			var data=JSON.parse(response);
			if(data){
				setCookie("auth_key",data['key']);
				for (var k=0;k<6;k++)
					setTempCookie("loginData"+k,data['ret'][k]);
			}else{
				window.location.href="index.html";
			}
		},
		error: function(){
			window.location.href="index.html";
		}
	});
	if (SU) //controllo su
	{
		if (getTempCookie('loginData4')==0)
			window.location.href="account.html";
	}
}

function logout()
{
	delCookie("auth_key");
	delCookie("auth_id");
	for (var k=0;k<6;k++)
		delTempCookie("loginData"+k);
	window.location.href="index.html";
}

function setCookie(name,value) {
	localStorage.setItem(name,value);
}
function getCookie(name) {
	return localStorage.getItem(name);
}
function delCookie(name) {
	localStorage.removeItem(name);
}

function setTempCookie(name,value) {
	sessionStorage.setItem(name,value);
}
function getTempCookie(name) {
	return sessionStorage.getItem(name);
}
function delTempCookie(name) {
	sessionStorage.removeItem(name);
}

function setRealCookie(name,value,exdays=1) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires=" + d.toGMTString();
	document.cookie = name + "=" + value + ";" + expires + ";path=/";
}
function getRealCookie(name) {
	name += "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1);
		if (c.indexOf(name) == 0)
			return c.substring(name.length, c.length);
	}
	return "";
}

function delRealCookie(name) {
	setRealCookie(name,"",-1);
}
