function beginningSet()
{
	//op could be: account_created     pwd_restored     account_deleted
	var op=getRealCookie('loginOption');
	if (op=='')	return;
	delRealCookie('loginOption');
	document.getElementById(op).style.display='block';
}

function showRegister()
{ 
	document.getElementById("login-form").style.display="none";
	document.getElementById("register-form").style.display="block";
	document.getElementById("read").checked=false;
}

function showLogin()
{
	document.getElementById("login-form").style.display="block";
	document.getElementById("register-form").style.display="none";
}

function checkInvio(event)
{
	var ev=event.keyCode;
	if (ev!=13)
		return;
	if (document.getElementById("login-form").style.display=='block')
	{
		if ($('#l-user').is(':focus') || $('#l-pwd').is(':focus'))
			login();
		else if ($('#user_email').is(':focus'))
			userRecovery();
		else if ($('#user_pwd').is(':focus'))	
			pwdRecovery();
	}
	else
		register();
}

function register()
{
	var user=document.getElementById("user").value ;
	var name=document.getElementById("name").value ;
	var pwd=document.getElementById("pwd").value ;
	var pwd2=document.getElementById("pwd2").value ;
	var email=document.getElementById('email').value;
	var read=document.getElementById('read').checked;
	
	var ret=false;
	
	if (user=="" )
	{
		ret=true;
		setNotify('user',$translation['insert-username'],'error','top center');
	}
	if (name=="")
	{
		ret=true;
		setNotify('name',$translation['insert-name'],'error','top center');	
	}
	if (email=="")
	{
		ret=true;
		setNotify('email',$translation['insert-mail'],'error','top center');
	}
	if (pwd=="")
	{
		ret=true;
		setNotify('pwd',$translation['insert-password'],'error','top center');
	}
	if (pwd2=="")
	{
		ret=true;
		setNotify('pwd2',$translation['confirm-password'],'error','top center');
	}
	if (!read)
	{
		ret=true;
		setNotify('readNotify',$translation['check'],'error','top center');	
	}
	if (ret) return;
	
	var tmp=checkUsername(user);
	if (tmp!=0)
	{
		setNotify('user',$translation['invalid-char']+" "+tmp,'error','top center');
		return;
	}
	if (!(validateEmail(email)))
	{
		setNotify('email',$translation['invalid-mail'],'error','top center');
		return;
	} 
	if (pwd!=pwd2)
	{
		setNotify('pwd',$translation['pwd-differs'],'error','top center');
		setNotify('pwd2',$translation['pwd-differs'],'error','top center');
		return;
	}
	
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			user: user,
			pwd: pwd,
			name: name,
			email: email,
			op: "register"  
		} ,
		success: function(response){
			var data=JSON.parse(response);
			if (data=='error1')
				setNotify('user',$translation['used-user'],'error','top center');
			else if (data=='error2')
				setNotify('email',$translation['used-mail'],'error','top center');
			else if (data=='error3')
				setNotify('submit',$translation['mail-error'],'error','top center');
			else if (data)
			{
				setNotify('submit',$translation['check-mail'],'success','top center');
				document.getElementById('user').value='';
				document.getElementById('name').value='';
				document.getElementById('email').value='';
				document.getElementById('pwd').value='';
				document.getElementById('pwd2').value='';
				document.getElementById('read').checked=false;
				$('#pwd').keyup();
			}
			else
				setNotify('submit',$translation['server-offline'],'error','top center');
		} ,
		error: function(response){
			setNotify('submit',$translation['server-offline'],'error','top center');
		}
	});
}

function login()
{
	var user =  document.getElementById("l-user").value ;
	var pwd=document.getElementById("l-pwd").value ;
	var ret=false;
	if (user=="")
	{
		setNotify('l-user',$translation['insert-username'],'error','top center');
		ret=true;
	}
	if (pwd=="")
	{
		setNotify('l-pwd',$translation['insert-password'],'error','top center');
		ret=true;
	}
	if (ret) return;
	
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			user: user,
			pwd: pwd,
			op: "login"  
		} ,
		success: function(response){
			var res=JSON.parse(response);
			var data = res['ret'];
			if(data[0]=='block')
			{
				var tmp=data[1].split(' ');
				var date=new Array();
				date[0]=tmp[0].split('-');
				date[1]=tmp[1].split(':');
				var dataTxt=date[0][2]+'/'+date[0][1]+'/'+date[0][0]+' '+date[1][0]+':'+date[1][1]; 
				setNotify('loginButton',$translation['banned-until']+' '+dataTxt,'error','top center');
			}
			else if(data[0]=='true')
			{
				setCookie("auth_key",res['key']);
				setCookie("auth_id",res['id']);
				for (var k=0;k<6;k++)
					setTempCookie("loginData"+k,data[k]);
				window.location.href="gruppi.html";
			}
			else
				setNotify('loginButton',$translation['incorrect-login'],'error','top center'); 
		} ,
		error: function(response){
			setNotify('loginButton',$translation['server-offline'],'error','top center');
		}
	});
}

function checkUsername(name)  //ret=0 tutto ok, altrimenti carattere non valido
{
	var chars="abcdefghijklmnopqrstuvwxyz1234567890.-_";
	name=name.toLowerCase();
	var ret;
	for (var k=0;k<name.length;k++)
	{
		ret=true;
		for (var i=0;i<chars.length;i++)
		{
			if (name[k]==' ')
				return "spazio";
			if (name[k]==chars[i])
				ret=false;
		}
		if (ret)
			return name[k];
	} 
	return 0;
}

function checkUserAvailability(){
	var user = $("#user").val();
	if(checkUsername(user)){
		for (var i = 0, len = user.length; i < len; i++) {
			if (!(/^[a-zA-Z0-9_\-\.]$/.test(user[i]))){
				user = user.replace(user[i]," ");
			}
		}
		user = user.replace(" ","");
		setNotify('user',$translation['invalid-char']+'\n'+$translation['try']+' '+user,'error','top center');
		return;
	}

	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			user: user,
			op: "userCheck"  
		} ,
		success: function(response){
			var data=JSON.parse(response);
			if(data)
				setNotify('user',$translation['used-user']+'\n'+$translation['try']+' '+ data,'error','top center');
		} ,
		error: function(response){
			setNotify('submit',$translation['server-offline'],'error','top center');
		}
	});
}

function checkEmailAvailability(){
	var email = $("#email").val();
	if(!validateEmail(email)){
		setNotify('email',$translation['invalid-mail'],'error','top center');
		return;
	}
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			email: email,
			op: "emailCheck"  
		} ,
		success: function(response){
			var data=JSON.parse(response);
			if(data)
				setNotify('email',$translation['used-mail'],'error','top center');
		} ,
		error: function(response){
			setNotify('submit',$translation['server-offline'],'error','top center');
		}
	});
}

//recupero di username e pwd

function clearInput()
{
	document.getElementById("user_email").value='';
	document.getElementById("user_pwd").value='';
}

function userRecovery()
{
	var email=$("#user_email").val();
	if (email=='')
	{
		setNotify('user_email',$translation['insert-mail'],'error','top center');
		return;
	}
	if (!validateEmail(email)){
		setNotify('user_email',$translation['invalid-mail'],'error','top center');
		return;
	}
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			email: email,
			op: "recoverUser"  
		} ,
		success: function(response){
			var data=JSON.parse(response);
			if (data=='error')
				setNotify('recoverUserNotify',$translation['mail-error'],'error','top center');
			else if(data)
			{
				setNotify('recoverUserNotify',$translation['send-mail'],'success','top center');
				clearInput();
			}
			else
				setNotify('recoverUserNotify',$translation['unfound-mail'],'error','top center');
		} ,
		error: function(response){
			setNotify('recoverUserNotify',$translation['server-offline'],'error','top center');
		}
	});
}

function pwdRecovery()
{
	var user=$("#user_pwd").val();
	if (user=='')
	{
		setNotify('user_pwd',$translation['insert-username'],'error','top center');
		return;
	}
	$.ajax({
		url:'../server/index.php',
		type: 'POST',
		data: { 
			user: user,
			op: "recoverPwd"  
		} ,
		success: function(response){
			var data=JSON.parse(response);
			if (data=='error')
				setNotify('recoverPwdNotify',$translation['mail-error'],'error','top center');
			else if(data)
			{
				setNotify('recoverPwdNotify',$translation['send-mail'],'success','top center');
				clearInput();
			}
			else
				setNotify('recoverPwdNotify',$translation['unfound-user'],'error','top center');
		} ,
		error: function(response){
			setNotify('recoverPwdNotify',$translation['server-offline'],'error','top center');
		}
	});
}
